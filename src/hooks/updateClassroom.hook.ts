import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from "react-router-dom";
import ClassroomsAPI from "../api/classrooms.api";
import { setClassroomInfo } from '../app/classroomSlice';

export default function useUpdateClassroom() {
  const { classroomUrlCode } = useParams<{ classroomUrlCode: string }>();
  const dispatch = useDispatch();

  const loadClassroomInfo = async () => {
    const res = await ClassroomsAPI.getInfoForClassroomPage(classroomUrlCode);
    if (res.data) {
      dispatch(setClassroomInfo(res.data));
    }
  }

  useEffect(() => {
    loadClassroomInfo();
  }, []);

  return loadClassroomInfo;
}
