import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import TasksAPI from '../api/tasks.api';
import { setTasks } from '../app/tasksSlice';

export default function useUpdateAllTasks() {
  const { classroomUrlCode, courseUrlCode } = useParams<{ classroomUrlCode: string, courseUrlCode: string }>();
  const dispatch = useDispatch();
  const history = useHistory();

  const [isLoading, setLoading] = useState(false);
  const [isError, setError] = useState(false);

  const loadAllTasks = async (showLoading?: boolean) => {
    if (showLoading) setLoading(true);
    setError(false);
    const { data, error } = await TasksAPI.getAllTasks(courseUrlCode, classroomUrlCode);
    if (data) {
      dispatch(setTasks(data));
      setLoading(false);
    } else if (error) {
      if (error.response && error.response.data.message === 'no classroom') {
        history.push('/app');
      } else if (error.response && error.response.data.message === 'no course') {
        history.push(`/app/${classroomUrlCode}`);
      }
      setError(true);
      setLoading(false);
    }
  }

  return [loadAllTasks, isLoading, isError] as [typeof loadAllTasks, boolean, boolean];
}
