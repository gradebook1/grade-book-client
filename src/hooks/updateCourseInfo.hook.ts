import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from "react-router-dom";
import CoursesAPI from '../api/courses.api';
import { setCourseInfo } from '../app/courseSlice';

export default function useUpdateCourseInfo() {
  const { classroomUrlCode, courseUrlCode } = useParams<{ classroomUrlCode: string, courseUrlCode: string }>();
  const dispatch = useDispatch();
  const history = useHistory();

  const [isLoading, setLoading] = useState(false);
  const [isError, setError] = useState(false);

  const loadCourseInfo = async (showLoading?: boolean) => {
    if (showLoading) {
      setLoading(true);
    }
    setError(false);
    const { data, error } = await CoursesAPI.getInfoForCoursePage(courseUrlCode, classroomUrlCode);
    if (data) {
      dispatch(setCourseInfo(data));
      setLoading(false);
    } else if (error) {
      if (error.response && error.response.data.message === 'no classroom') {
        history.push('/app');
      } else if (error.response && error.response.data.message === 'no course') {
        history.push(`/app/${classroomUrlCode}`);
      }
      // TODO: Handle here 403 forbidden error to show that user doesnt have access to that private course
      setError(true);
      setLoading(false);
    }
  }

  return [loadCourseInfo, isLoading, isError] as [typeof loadCourseInfo, boolean, boolean];
}
