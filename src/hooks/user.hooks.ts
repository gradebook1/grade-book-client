import { useEffect } from "react";
import { useHistory } from "react-router";
import { IUserData } from "../app/userSlice";

export function useAuthGuard() {
  const history = useHistory();

  useEffect(() => {
    if (!localStorage.getItem('userData')) {
      history.push('/');
    }
  });
}

export function useUserData() {
  const userDataString = localStorage.getItem('userData');
  if (userDataString) {
    return JSON.parse(userDataString) as IUserData;
  }
}