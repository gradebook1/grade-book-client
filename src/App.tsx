import React, { lazy, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.less';
import { LoaderPage } from './components/Loader/Loader';

const HomePage = lazy(() => import('./pages/Home.page'));
const AppPage = lazy(() => import('./pages/App.page'));

function App() {
  return (
    <Router basename="/grade-book-client">
      <Suspense fallback={<LoaderPage/>}>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/app" component={AppPage} />
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
