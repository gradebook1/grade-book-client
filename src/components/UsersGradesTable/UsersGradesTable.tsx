// import React, { useState } from 'react';
// import { Table, Radio, Divider } from 'antd';
// import { IUserData } from '../../app/userSlice';

// const columns = [
//   {
//     title: `Студент`,
//     dataIndex: 'name',
//     // render: (text: string) => <a>{text}</a>, // TODO: Add links to user's profiles 
//   },
//   {
//     title: 'Оцінка',
//     dataIndex: 'grade',
//   },
//   // {
//   //   title: 'Address',
//   //   dataIndex: 'address',
//   // },
// ];

// interface DataType {
//   key: React.Key;
//   name: string;
//   grade: number;
//   // age: number;
//   // address: string;
// }

// interface IUsersGradesTableProps {
//   users?: IUserData[];
//   onChange?: (selectedRowKeys: React.Key[]) => any;
// }

// const UsersGradesTable = ({ users, onChange } : IUsersGradesTableProps) => {
//   return (
//     <div>
//       <Table
//         columns={columns}
//         dataSource={users?.map(user => ({ key: user._id, name: `${user.firstName} ${user.lastName}`, grade: 0 } as DataType))}
//         pagination={false}
//       />
//     </div>
//   );
// };

// export default UsersGradesTable;



// EDITABLE TABLE 

import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, InputNumber } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { IUserData } from '../../app/userSlice';
import './UsersGradesTable.css';
import { IGrade } from '../../api/tasks.api';

const EditableContext = React.createContext<FormInstance<any> | null>(null);

interface Item {
  key: string;
  name: string;
  grade: number;
  autoGrade: number;
}

interface EditableRowProps {
  index: number;
}

const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface EditableCellProps {
  title: React.ReactNode;
  editable: boolean;
  children: React.ReactNode;
  dataIndex: keyof Item;
  record: Item;
  handleSave: (record: Item) => void;
}

const EditableCell: React.FC<EditableCellProps> = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  // const inputRef = useRef<Input>(null);
  const inputRef = useRef<any>(null);
  const form = useContext(EditableContext)!;

  useEffect(() => {
    if (editing) {
      inputRef.current!.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({ [dataIndex]: record[dataIndex] });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();

      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{ marginBottom: 0, marginTop: 0, marginLeft: 0, marginRight: 0 }}
        name={dataIndex}
        // rules={[
        //   {
        //     required: true,
        //     message: `${title} is required.`,
        //   },
        // ]}
      >
        {/* <Input ref={inputRef} onPressEnter={save} onBlur={save} /> */}
        <InputNumber className="w-full" ref={inputRef} min={0} type="number" onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div className="editable-cell-value-wrap h-8" style={{ paddingRight: 24 }} onClick={toggleEdit}>
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

type EditableTableProps = Parameters<typeof Table>[0] & { columns: any, onGradeChange?: (userId: string, grade?: number) => any };

interface DataType {
  key: React.Key;
  name: string;
  grade?: number;
  autoGrade?: number;
}

interface EditableTableState {
  dataSource: DataType[];
  count: number;
}

type ColumnTypes = Exclude<EditableTableProps['columns'], undefined>;

class EditableTable extends React.Component<EditableTableProps, EditableTableState> {
  columns: (ColumnTypes[number] & { editable?: boolean; dataIndex: string })[];

  constructor(props: EditableTableProps) {
    super(props);

    this.columns = this.props.columns;

    this.state = {
      dataSource: this.props.dataSource as DataType[],
      count: 2,
    };
  }

  componentDidUpdate(prevProps: Readonly<EditableTableProps>, prevState: Readonly<EditableTableState>) {
    if (prevProps.dataSource !== this.props.dataSource) {
      this.setState({ dataSource: this.props.dataSource as DataType[] });
      // console.log('reload grades');
    }
  }

  handleSave = (row: DataType) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    this.setState({ dataSource: newData });
    this.props.onGradeChange && this.props.onGradeChange(row.key.toString(), row.grade);
  };

  render() {
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: (record: DataType) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns as ColumnTypes}
          pagination={false}
        />
      </div>
    );
  }
}

interface IUsersGradesTableProps {
  users?: IUserData[];
  grades?: IGrade[];
  autoGrades?: IGrade[];
  onGradeChange?: (userId: string, grade?: number) => any;
}

const UsersGradesTable = ({ users, grades, autoGrades, onGradeChange } : IUsersGradesTableProps) => {
  const columns = autoGrades ? (
    [
      {
        title: 'Студент',
        dataIndex: 'name',
      },
      {
        title: 'Вирахувана оцінка',
        dataIndex: 'autoGrade',
        width: '25%',
      },
      {
        title: 'Оцінка',
        dataIndex: 'grade',
        width: '25%',
        editable: true,
      },
    ]
  ) : (
    [
      {
        title: 'Студент',
        dataIndex: 'name',
      },
      {
        title: 'Оцінка',
        dataIndex: 'grade',
        width: '30%',
        editable: true,
      },
    ]
  );

  const dataSource = autoGrades ? (
    users?.map(user => ({ key: user._id, name: `${user.firstName} ${user.lastName}`, grade: grades?.find(grade => grade.student === user._id)?.grade, autoGrade: autoGrades?.find(grade => grade.student === user._id)?.grade } as DataType))
  ) : (
    users?.map(user => ({ key: user._id, name: `${user.firstName} ${user.lastName}`, grade: grades?.find(grade => grade.student === user._id)?.grade } as DataType))
  );

  return (
    <div>
      <EditableTable
        columns={columns}
        dataSource={dataSource}
        pagination={false}
        onGradeChange={onGradeChange}
      />
    </div>
  );
};

export default UsersGradesTable;
