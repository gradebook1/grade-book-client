import { Button } from 'antd';
import styled from 'styled-components';

export const IconButton = styled(Button)`
  display: flex;
  justify-content: center;
  align-items: center;
`;
