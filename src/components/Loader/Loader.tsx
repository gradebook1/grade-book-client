import React from 'react'
import styled from 'styled-components';
import styles from './Loader.module.css';

function Loader({ className } : { className?: string }) {
  return (
    <div className={`h-full py-10 flex justify-center items-center ${className}`}>
      <div className={styles['lds-roller']}><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div>
  );
}

export const DarkLoader = styled(Loader)`
  & > div div:after {
    background: gray;
  }
`;

export function LoaderPage() {
  return (
    <div className="min-h-screen bg-gray-800 flex flex-col justify-center items-center">
      <h1 className="font-bold text-3xl text-gray-50 mb-2">GradeBook</h1>
      <div className={styles['lds-roller']}><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    </div>
  );
}

export default Loader;