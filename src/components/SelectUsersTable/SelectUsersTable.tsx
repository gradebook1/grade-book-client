import React, { useState } from 'react';
import { Table, Radio, Divider } from 'antd';
import { IUserData } from '../../app/userSlice';

const columns = [
  {
    title: `Виберіть студентів`,
    dataIndex: 'name',
    // render: (text: string) => <a>{text}</a>,
  },
  // {
  //   title: 'Age',
  //   dataIndex: 'age',
  // },
  // {
  //   title: 'Address',
  //   dataIndex: 'address',
  // },
];

interface DataType {
  key: React.Key;
  name: string;
  // age: number;
  // address: string;
}

// const data: DataType[] = [
//   {
//     key: '1',
//     name: 'John Brown',
//     age: 32,
//     address: 'New York No. 1 Lake Park',
//   },
//   {
//     key: '2',
//     name: 'Jim Green',
//     age: 42,
//     address: 'London No. 1 Lake Park',
//   },
//   {
//     key: '3',
//     name: 'Joe Black',
//     age: 32,
//     address: 'Sidney No. 1 Lake Park',
//   },
//   {
//     key: '4',
//     name: 'Disabled User',
//     age: 99,
//     address: 'Sidney No. 1 Lake Park',
//   },
// ];

// rowSelection object indicates the need for row selection
const getRowSelection = (afterChange?: (selectedRowKeys: React.Key[]) => any) => ({
  onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
    // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    afterChange && afterChange(selectedRowKeys);
  },
  getCheckboxProps: (record: DataType) => ({
    // disabled: record.name === 'Disabled User', // Column configuration not to be checked
    name: record.name,
  }),
});

interface ISelectUsersTableProps {
  selectionType: 'checkbox' | 'radio';
  users: IUserData[];
  selectedUsers?: string[];
  onChange?: (selectedRowKeys: React.Key[]) => any;
}

const SelectUsersTable = ({ selectionType, users, onChange, selectedUsers } : ISelectUsersTableProps) => {
  // const [selectionType, setSelectionType] = useState<'checkbox' | 'radio'>('checkbox');

  return (
    <div>
      {/* <Radio.Group
        onChange={({ target: { value } }) => {
          setSelectionType(value);
        }}
        value={selectionType}
      >
        <Radio value="checkbox">Checkbox</Radio>
        <Radio value="radio">radio</Radio>
      </Radio.Group> */}
      {/* <Divider /> */}
      <Table
        rowSelection={{
          type: selectionType,
          selectedRowKeys: selectedUsers,
          ...getRowSelection(onChange),
        }}
        columns={columns}
        dataSource={users.map(user => ({ key: user._id, name: `${user.firstName} ${user.lastName}` } as DataType))}
        pagination={false}
      />
    </div>
  );
};

export default SelectUsersTable;