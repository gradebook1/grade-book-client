import { Tag, Tooltip } from 'antd';
import React from 'react';

const getAlign = (align: string = 'center') => {
  if (align === 'center') return 'self-center';
  else if (align === 'start') return 'self-start';
  else if (align === 'end') return 'self-end';
}

interface PTagProps {
  text?: string;
  children?: any;
  align?: 'center' | 'start' | 'end';
  tooltip?: string;
}

export default function PTag({ text, children, tooltip, align = 'center' } : PTagProps) {
  const tag = (
    <Tag
      color="cyan"
      className={`bg-transparent text-green-500 border-green-500 ${getAlign(align)} text-sm`}
    >
      {text || children}
    </Tag>
  );

  return tooltip ? (
    <Tooltip title={tooltip}>
      {tag}
    </Tooltip>
  ) : tag;
}