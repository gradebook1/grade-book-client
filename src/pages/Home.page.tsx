import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'antd';
import LoginForm from '../features/loginForm/LoginForm';
import { useHistory } from 'react-router';
import RegisterForm from '../features/registerForm/RegisterForm';

function HomePage() {
  const [loginModalVisible, setLoginModalVisible] = useState(false);
  const [registerModalVisible, setRegisterModalVisible] = useState(false);

  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem('userData')) {
      history.push('/app');
    }
  });

  return (
    <div className="bg-gray-800 min-h-screen flex justify-center items-center">
      <div className="px-4 flex flex-col items-center">
        <h1 className="font-bold text-3xl md:text-6xl text-gray-50 mb-1">GradeBook</h1>
        <p className="md:text-lg text-center font-medium tracking-wider text-green-500">Система управління вашими оцінками.</p>
        <div className="flex flex-col md:flex-row p-4 space-y-4 md:space-y-0 md:space-x-4">
          <Button type="primary" size="large" onClick={() => setLoginModalVisible(true)}>Увійти</Button>
          <Button type="primary" ghost size="large" onClick={() => setRegisterModalVisible(true)}>Зареєструватися</Button>
        </div>
      </div>
      <Modal
        title="Увійти"
        centered
        footer={null}
        visible={loginModalVisible}
        onOk={() => setLoginModalVisible(false)}
        onCancel={() => setLoginModalVisible(false)}
      >
        <LoginForm />
      </Modal>
      <Modal
        title="Зареєструватися"
        centered
        footer={null}
        visible={registerModalVisible}
        onOk={() => setRegisterModalVisible(false)}
        onCancel={() => setRegisterModalVisible(false)}
      >
        <RegisterForm onRegistered={() => setRegisterModalVisible(false)} />
      </Modal>
    </div>
  );
}

export default HomePage;