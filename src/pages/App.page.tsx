import { Button, Layout, Modal } from 'antd';
import React, { useState } from 'react';
import { Link, Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import { IUserData } from '../app/userSlice';
import ClassroomsBlock from '../features/classrooms/ClassroomsList';
import EditProfileForm from '../features/editProfileForm/ProfileForm';
import { useAuthGuard } from '../hooks/user.hooks';
import ClassroomSubPage from './Classroom.sub.page';

const { Header, Content } = Layout;

function AppPage() {
  useAuthGuard();
  const history = useHistory();
  const match = useRouteMatch();
  const [editProfileModalVisible, setEditProfileModalVisible] = useState(false);

  const userData: IUserData = JSON.parse(localStorage.getItem('userData')!);

  const logout = () => {
    localStorage.removeItem('userData');
    history.push('/');
    // TODO: Clear token cookie ?
  }

  const openProfile = () => {
    setEditProfileModalVisible(true);
  }

  return (
    <>
    <div className="bg-gray-800 min-h-screen text-gray-300">
      <Layout className="min-h-screen">
        <Header className="flex items-center bg-gray-800">
          <Link to="/app" className="font-bold text-3xl text-gray-50 mb-0">GradeBook</Link>
          <div className="ml-auto space-x-3">
            <span className="text-gray-300 font-semibold text-base">
              {`${userData?.firstName} ${userData?.lastName}`}
            </span>
            <Button type="primary" onClick={() => openProfile()}>Профіль</Button>
            <Button type="primary" ghost onClick={() => logout()}>Вийти</Button>
            {/* TODO: Add help button here */}
          </div>
        </Header>
        <Content className="bg-gray-800 text-gray-300">
          <Switch>
            <Route exact path={`${match.path}`} >
              <ClassroomsBlock />
            </Route>
            <Route path={`${match.path}/:classroomUrlCode`} >
              <ClassroomSubPage />
            </Route>
          </Switch>
        </Content>
      </Layout>
    </div>
    <Modal
      title="Редагувати профіль"
      centered
      footer={null}
      visible={editProfileModalVisible}
      onCancel={() => setEditProfileModalVisible(false)}
    >
      <EditProfileForm onEditProfile={() => {
        setEditProfileModalVisible(false);
      }} />
    </Modal>
    </>
  );
}

export default AppPage;