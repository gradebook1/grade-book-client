import { PlusCircleFilled } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, useHistory, useParams, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import ClassroomsAPI from '../api/classrooms.api';
import { ICourseShortInfo } from '../api/courses.api';
import { deleteSelectedCourseShortInfo, selectClassroom, selectSelectedCourseShortInfo, selectUserClassroomRole, setClassroomInfo, setSelectedCourseShortInfo, UserClassroomRole } from '../app/classroomSlice';
import { deleteCourseInfo, selectCourse } from '../app/courseSlice';
import Loader from '../components/Loader/Loader';
import { ClassroomHeader } from '../features/classroomHeader/ClassroomHeader';
import ClassroomSettings from '../features/classroomSettings/ClassroomSettings';
import CreateCourseForm from '../features/createCourseForm/createCourseForm';
import UsersList from '../features/usersList/UsersList';
import { useAuthGuard } from '../hooks/user.hooks';
import CourseSubPage from './Course.sub.page';

export default function ClassroomSubPage() {
  useAuthGuard();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const userClassroomRole = useSelector(selectUserClassroomRole);
  const selectedCourseShortInfo = useSelector(selectSelectedCourseShortInfo);

  const [isLoading, setLoading] = useState(true);
  const [isError, setError] = useState(false);
  const [createCourseModalVisible, setCreateCourseModalVisible] = useState(false);
  const [editCourseModalVisible, setEditCourseModalVisible] = useState(false);

  const { classroomUrlCode } = useParams<{ classroomUrlCode: string }>();
  const dispatch = useDispatch();
  const match = useRouteMatch();
  const history = useHistory();

  const coursePageRef = useRef<{ loadCourseInfo: (silent?: boolean) => any }>();

  const loadClassroomInfo = async () => {
    const { data, error } = await ClassroomsAPI.getInfoForClassroomPage(classroomUrlCode);
    if (data) {
      dispatch(setClassroomInfo(data));
      setLoading(false);
    } else if (error) {
      if (error.response && error.response.data.message === 'no classroom') {
        history.push('/app');
      }
      setError(true);
      setLoading(false);
    }
  }

  const handleCourseButtonClick = (course: ICourseShortInfo) => {
    dispatch(setSelectedCourseShortInfo(course));
    history.push(`/app/${classroomUrlCode}/c/${course.urlCode}`);
  }

  const handleEditCourse = () => {
    setEditCourseModalVisible(true);
  }

  useEffect(() => {
    loadClassroomInfo();
    return () => {
      dispatch(deleteSelectedCourseShortInfo());
    }
  }, []);

  let content;
  if (isLoading) {
    content = <Loader />;
  } else if (isError) {
    content = <h2 className="text-gray-300 text-center">Помилка при завантажені даних. Спробуйте пізніше.</h2>
  } else if (classroomInfo) {
    content = (
      <div className="flex flex-row items-start">
        <section className="w-60 pt-5 flex-shrink-0 sticky top-0">
          <h1 className="text-gray-300 text-center text-lg">Курси</h1>
          <div className="flex flex-col items-center space-y-4">
            {/* <button className="bg-gray-700 hover:bg-green-500 px-4 py-3 w-40 rounded-3xl hover:rounded-xl transition-all duration-300 outline-none shadow-lg">
              <h2 className="text-gray-50 mb-0 truncate">Фізика</h2>
            </button>
            <button className="bg-gray-700 hover:bg-green-500 px-4 py-3 w-40 rounded-3xl hover:rounded-xl transition-all duration-300 outline-none shadow-lg">
              <h2 className="text-gray-50 mb-0 truncate">Фізика</h2>
            </button> */}
            {classroomInfo.courses.map(course => <StyledCourseButton key={course._id} onClick={() => handleCourseButtonClick(course)} course={course} active={course._id === selectedCourseShortInfo?._id || course._id === courseInfo?._id}/>)}
            {/* <StyledCourseButton active onClick={() => {}} course={{name: 'Математика'} as ICourseShortInfo}/> */}
            {/* <StyledCourseButton name='Фізика' active/>
            <StyledCourseButton name='Українська мова'/> */}
            {userClassroomRole === UserClassroomRole.CLASSHEAD && <Button onClick={() => setCreateCourseModalVisible(true)} icon={<PlusCircleFilled />} type="dashed" ghost className="flex justify-center items-center px-4 py-3 h-auto w-44 rounded-3xl hover:rounded-xl transition-all duration-300">Новий курс</Button>}
          </div>
        </section>
        <section className="p-4 flex-auto rounded-tl-lg rounded-bl-lg space-y-5">
          <ClassroomHeader />
          <Switch>
            <Route path={`${match.path}/users`}>
              <UsersList />
            </Route>
            <Route path={`${match.path}/settings`}>
              <ClassroomSettings />
            </Route>
            <Route path={`${match.path}/c/:courseUrlCode`}>
              <CourseSubPage ref={coursePageRef} onEditCourse={handleEditCourse} />
            </Route>
            <Route path={match.path}>
              <div className="text-center pt-10">
                <h2 className="text-gray-300 text-lg">Виберіть курс зі списку</h2>
              </div>
            </Route>
          </Switch>
        </section>
      </div>
    );
  }

  return (
    <div>
      {content}
      <Modal
        title="Новий курс"
        centered
        footer={null}
        visible={createCourseModalVisible}
        onCancel={() => setCreateCourseModalVisible(false)}
      >
        <CreateCourseForm onCourseCreated={() => {
          setCreateCourseModalVisible(false);
          loadClassroomInfo();
        }} />
      </Modal>
      <Modal
        title="Налаштування курсу"
        centered
        footer={null}
        visible={editCourseModalVisible}
        onCancel={() => setEditCourseModalVisible(false)}
      >
        <CreateCourseForm editMode onCourseCreated={() => {
          setEditCourseModalVisible(false);
          loadClassroomInfo();
          // history.push(match.url + `/c/${courseInfo?.urlCode}`);
          coursePageRef.current?.loadCourseInfo(true);
        }} />
      </Modal>
    </div>
  );
}

function CourseButton(props: {
  active?: boolean,
  course: ICourseShortInfo,
  className?: string,
  onClick: (event?: any) => any,
}) {
  const { name } = props.course;

  return (
    <div className={`relative ${props.className}`}>
      <button onClick={props.onClick} className={`${props.active ? 'bg-green-500 rounded-xl' : 'bg-gray-700 rounded-3xl'} hover:bg-green-500 px-4 py-3 w-44 hover:rounded-xl transition-all duration-300 outline-none shadow-lg`}>
        <h2 className="text-white mb-0 truncate">{name}</h2>
      </button>
      <div className={`${props.active ? 'opacity-100' : 'opacity-0 transform scale-y-50'} absolute top-0 bottom-0 -left-3 w-1 rounded-full bg-green-500 transition-all`}></div>
    </div>
  );
}

export const StyledCourseButton = styled(CourseButton)`
  & > button:hover + div {
    opacity: 1 !important;
  }
`;