import { Button, Modal } from 'antd';
import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch, useHistory, useParams, useRouteMatch } from 'react-router-dom';
import CoursesAPI from '../api/courses.api';
import { GradeCalculationType } from '../api/tasks.api';
import { selectUserClassroomRole, UserClassroomRole } from '../app/classroomSlice';
import { deleteCourseInfo, selectCourse, selectUserCourseRole, setCourseInfo, UserCourseRole } from '../app/courseSlice';
import { selectGroups, selectTasks } from '../app/tasksSlice';
import Loader from '../components/Loader/Loader';
import PTag from '../components/Tag';
import CourseGrades from '../features/grades/CourseGrades';
import TasksBlock from '../features/tasks/TasksBlock';
import useUpdateAllTasks from '../hooks/updateAllTasks.hook';
import useUpdateCourseInfo from '../hooks/updateCourseInfo.hook';
import { useAuthGuard } from '../hooks/user.hooks';
import { getUserCourseGrade } from '../services/getCourseGrade';

interface ICourseSubPageProps {
  onEditCourse?: () => any;
}

function CourseSubPage({ onEditCourse } : ICourseSubPageProps, ref: any) {
  useAuthGuard();
  const courseInfo = useSelector(selectCourse);
  const userCourseRole = useSelector(selectUserCourseRole);
  const userClassroomRole = useSelector(selectUserClassroomRole);
  const tasks = useSelector(selectTasks);
  const groups = useSelector(selectGroups);

  const [isLoading, setLoading] = useState(true);
  const [isError, setError] = useState(false);
  const [prevCourseUrlCode, setPrevCourseUrlCode] = useState<string>();

  const { classroomUrlCode, courseUrlCode } = useParams<{ classroomUrlCode: string, courseUrlCode: string }>();
  const dispatch = useDispatch();
  const match = useRouteMatch();
  const history = useHistory();

  const [gradesModalVisible, setGradesModalVisible] = useState(false);

  const [updateAllTasks] = useUpdateAllTasks();
  const [updateCourse] = useUpdateCourseInfo();

  const loadCourseInfo = async (silent?: boolean) => {
    if (!silent) {
      setLoading(true);
    }
    setError(false);
    const { data, error } = await CoursesAPI.getInfoForCoursePage(courseUrlCode, classroomUrlCode);
    if (data) {
      dispatch(setCourseInfo(data));
      setLoading(false);
    } else if (error) {
      if (error.response && error.response.data.message === 'no classroom') {
        history.push('/app');
      } else if (error.response && error.response.data.message === 'no course') {
        history.push(`/app/${classroomUrlCode}`);
      }
      // TODO: Handle here 403 forbidden error to show that user doesnt have access to that private course
      setError(true);
      setLoading(false);
    }
    setPrevCourseUrlCode(courseUrlCode);
  }

  const handleGradesClick = () => {
    //history.push(`/app/${classroomUrlCode}/c/${courseInfo?.urlCode}/cg`);
    setGradesModalVisible(true);
  };

  useEffect(() => {
    loadCourseInfo();
    return () => {
      dispatch(deleteCourseInfo());
    };
  }, [courseUrlCode]);

  useImperativeHandle(ref, () => ({
    loadCourseInfo,
  }));

  let content;
  if (isLoading) {
    content = <Loader />;
  } else if (isError) {
    content = <h2 className="text-gray-300 text-center">Помилка при завантажені даних. Спробуйте пізніше.</h2>
  } else if (courseInfo && courseUrlCode === prevCourseUrlCode) {
    content = (
      <div className="flex flex-row space-x-5 items-start">
        <section className="bg-gray-700 p-5 max-w-xs flex-auto rounded-lg shadow-lg flex flex-col sticky top-5">
          <h3 className="text-gray-50 text-center text-xl">{courseInfo.name}</h3>
          <p>Дата створення: <span className="text-gray-50">{new Date(courseInfo.createdDate).toLocaleDateString()}</span></p>
          <p>Викладач: <span className="text-gray-50">{`${courseInfo.teacher.firstName} ${courseInfo.teacher.lastName}`}</span></p>
          {userCourseRole === UserCourseRole.STUDENT && tasks && (
            <>
            <p>Вирахувана оцінка за курс: <span className="text-gray-50">{getUserCourseGrade(courseInfo.gradeType, tasks, groups)}</span></p>
            <p>Оцінка за курс: <span className="text-gray-50">{courseInfo.grades[0]?.grade}</span></p>
            </>
          )}
          <p>Спосіб вирахування оцінки: <span className="text-gray-50">{courseInfo.gradeType === GradeCalculationType.AVERAGE ? 'Середнє арифметичне всіх оцінок' : (courseInfo.gradeType === GradeCalculationType.SUM && 'Сума всіх оцінок')}</span></p>
          {courseInfo.isPrivate && <p className="text-center"><PTag>Приватний курс</PTag></p>}
          <div className="flex flex-col space-y-3">
            {(userClassroomRole === UserClassroomRole.CLASSHEAD) && (
              <Button type="primary" onClick={onEditCourse}>Налаштування курсу</Button>
            )}
            {userCourseRole === UserCourseRole.COURSE_TEACHER && (
              <Button type="primary" onClick={handleGradesClick}>Оцінки курсу</Button>
            )}
          </div>
        </section>
        <section className="bg-gray-700 p-5 flex-auto rounded-lg shadow-lg">
          <Switch>
            <Route exact path={`${match.path}`}>
              <TasksBlock />
            </Route>
            {/* <Route path={`${match.path}/tg`}>
              <TaskGrades />
            </Route> */}
            {/* <Route path={`${match.path}/cg`}>
              <CourseGrades />
            </Route> */}
            {/* <Route path={`${match.path}/gg`}>
              <GroupGrades />
            </Route> */}
            {/* <Route path={`${match.path}/sgg`}>
              <SubgroupGrades />
            </Route> */}
            {/* <Route path={`${match.path}/mg`}>
              <ModuleGrades />
            </Route> */}
          </Switch>
        </section>
      </div>
    );
  }

  return (
    <>
    <div>
      {content}
    </div>
    <Modal
      width={620}
      title="Оцінки"
      centered
      footer={null}
      visible={gradesModalVisible}
      onCancel={() => {
        setGradesModalVisible(false);
        updateAllTasks();
        updateCourse();
        // dispatch(deleteSelectedTaskForGrades());
      }}
    >
      <CourseGrades />
    </Modal>
    </>
  );
}

export default forwardRef(CourseSubPage);
