import { Button, Form, Input, message } from 'antd';
import React from 'react';
import styled from 'styled-components';
import ClassroomsAPI from '../../api/classrooms.api';

const CenteredFormItem = styled(Form.Item)`
  & .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }
`;

export interface IJoinClassroomFormProps {
  onJoined: () => any;
}

export default function JoinClassroomForm({ onJoined }:IJoinClassroomFormProps) {
  const [form] = Form.useForm();

  const onFinish = async (values: any) => {
    const { error, data } = await ClassroomsAPI.joinClassroom(values);
    if (data) {
      form.resetFields();
      onJoined();
      message.success(`Ви успішно приєдналися до класу "${data.name}"`);
    } else if (error) {
      if (!error.response || error.response.status !== 400) {
        message.error('Виникла помилка! Спробуйте пізніше.');
        return;
      }
      const errorMessage = error.response.data.message;
      if (errorMessage === 'classroom') {
        message.error('Не знайдено клас за цим кодом!');
      } else if (errorMessage === 'user is classhead') {
        message.error('Ви не можете бути студентом в створеному вами класі!');
      } else if (errorMessage === 'already joined') {
        message.error('Ви вже навчаєтесь в цьому класі!');
      } else if (errorMessage === 'user is teacher') {
        message.error('Ви не можете навчатись в класі, в якому викладаєте!');
      }
    }
  };

  return (
    <Form onFinish={onFinish} form={form}>
      <Form.Item
        name="code"
        label="Код класу"
        help="Код класу можна дізнатися у керівника класу."
        rules={[{required: true}]}
      >
        <Input placeholder="Код класу" className="max-w-xs"/>
      </Form.Item>
      <CenteredFormItem className="mb-0 mt-4">
        <Button type="primary" htmlType="submit">
          Приєднатися
        </Button>
      </CenteredFormItem>
    </Form>
  );
}
