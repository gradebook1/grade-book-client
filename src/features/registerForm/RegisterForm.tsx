import React from 'react';
import { Form, Input, Button, message, Modal } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import AuthAPI, { ICreateUserDTO } from '../../api/auth.api';

export default function RegisterForm({ onRegistered } : { onRegistered?: () => any }) {
  const [form] = Form.useForm();

  const onFinish = async (values: ICreateUserDTO) => {
    const { data, error } = await AuthAPI.register(values);
    if (data) {
      Modal.success({
        title: 'Реєстрація',
        content: 'Ви успішно зареєструвалися і тепер можете увійти у свій аккаунт.',
        centered: true,
        okText: 'Закрити',
      });
      form.resetFields();
      onRegistered && onRegistered();
    } else if (error) {
      if (error.response && error.response.data.message === 'user exists') {
        message.error('Ця електронна пошта вже зайнята іншим користувачем!.');
      } else {
        message.error('Виникла помилка! Спробуйте пізніше.');
      }
    }
  };

  return (
    <Form
      form={form}
      name="register"
      className="login-form"
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          { required: true, message: 'Введіть свою електронну пошту!' },
          { pattern: /^\S+@\S+.\S+$/, message: 'Щось це не схоже на електронну пошту...' }
        ]}
      >
        <Input type="email" prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Електронна пошта" />
      </Form.Item>
      <Form.Item
        name="firstName"
        rules={[
          { required: true, message: 'Введіть своє ім\'я!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Ім'я"
        />
      </Form.Item>
      <Form.Item
        name="lastName"
        rules={[
          { required: true, message: 'Введіть своє прізвище!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Прізвище"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          { required: true, message: 'Введіть пароль!' },
          { min: 6, message: 'Мінімум 6 символів!' }
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Пароль"
        />
      </Form.Item>
      <Form.Item
        name="password-confirm"
        rules={[
          { required: true, message: 'Підтвердіть пароль!' },
          { min: 6, message: 'Мінімум 6 символів!' },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('Паролі не співпадають!'));
            },
          }),
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Підтвердіть пароль"
        />
      </Form.Item>
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit" className="w-full">
          Зареєструватися
        </Button>
      </Form.Item>
    </Form>
  );
}