import { PlusCircleFilled, SettingFilled } from '@ant-design/icons';
import { Collapse, Button, Modal, Tooltip } from 'antd';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { IGroupPopulated } from '../../api/groups.api';
import { ISubgroupPopulated } from '../../api/subgroups.api';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectUserCourseRole, UserCourseRole } from '../../app/courseSlice';
import { selectGroups, selectSelectedGroupForModal, selectSelectedSubgroupForModal, setSelectedSubgroupForGrades, setSelectedSubgroupForModal } from '../../app/tasksSlice';
import { IconButton } from '../../components/FixedIconButton';
import useUpdateAllTasks from '../../hooks/updateAllTasks.hook';
import useUpdateCourseInfo from '../../hooks/updateCourseInfo.hook';
import { getUserSubgroupGrade } from '../../services/getCourseGrade';
import CreateModuleForm from '../createModuleForm/createModuleForm';
import CreateSubgroupForm from '../createSubgroupForm/createSubgroupForm';
import CreateTaskForm from '../createTaskForm/createTaskForm';
import SubgroupGrades from '../grades/SubgroupGrades';
import ModulesList from './ModulesList';
import TasksList from './TasksList';

const { Panel } = Collapse;

const StyledPanel = styled(Panel)`
  & .ant-collapse-arrow {
    color: white !important;
  }
`;

interface ISubgroupsListProps {
  subgroups: ISubgroupPopulated[];
  onChange?: () => any;
  groupProp?: IGroupPopulated;
}

export default function SubgroupsList({ subgroups, onChange, groupProp } : ISubgroupsListProps) {
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  const userCourseRole = useSelector(selectUserCourseRole);
  const selectedGroupForModal = useSelector(selectSelectedGroupForModal);
  const selectedSubgroupForModal = useSelector(selectSelectedSubgroupForModal);
  const allGroups = useSelector(selectGroups);

  const [createTaskModalVisible, setCreateTaskModalVisible] = useState(false);
  const [createModuleModalVisible, setCreateModuleModalVisible] = useState(false);

  const [editSubgroupModalVisible, setEditSubgroupModalVisible] = useState(false);

  const [gradesModalVisible, setGradesModalVisible] = useState(false);

  const [updateAllTasks] = useUpdateAllTasks();
  const [updateCourse] = useUpdateCourseInfo();

  const handleCreateTask = (event: any, subgroup: ISubgroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedSubgroupForModal(subgroup));
    setCreateTaskModalVisible(true);
  }

  const handleCreateModule = (event: any, subgroup: ISubgroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedSubgroupForModal(subgroup));
    setCreateModuleModalVisible(true);
  }

  const handleGradesClick = (event: any, subgroup: ISubgroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedSubgroupForGrades(subgroup));
    setGradesModalVisible(true);
  };

  const handleEdit = (event: any, subgroup: ISubgroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedSubgroupForModal(subgroup));
    setEditSubgroupModalVisible(true);
  };


  return (
    <>
    {subgroups.length > 0 && (
      <Collapse ghost className="space-y-3 rounded border-none shadow-lg">
        {subgroups.map(subgroup => {
          return (
            <StyledPanel
              className="bg-gray-800 rounded"
              key={subgroup._id}
              header={
                <>
                <span className="text-gray-400 text-xs">Підгрупа </span>
                <span className="text-gray-50">{subgroup.name}</span>
                </>
              }
              extra={
                <>
                <div className="flex ml-auto space-x-4">
                  {userCourseRole === UserCourseRole.COURSE_TEACHER && (
                    <>
                    <Button size="small" shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={event => handleCreateTask(event, subgroup)}>Створити завдання</Button>
                    <Button size="small" shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={event => handleCreateModule(event, subgroup)}>Створити модуль</Button>
                    <Button type="primary" size="small" onClick={event => handleGradesClick(event, subgroup)}>Оцінки</Button>
                    <Tooltip title="Редагувати підгрупу">
                      <IconButton ghost type="link" size="small" shape="circle" icon={<SettingFilled />} onClick={event => handleEdit(event, subgroup)} />
                    </Tooltip>
                    </>
                  )}
                  {userCourseRole === UserCourseRole.STUDENT && (
                    <>
                    <Tooltip title="Вирахувана оцінка за підгрупу">
                      <div className="h-full bg-gray-700 px-3 w-20 text-center text-gray-300 rounded shadow-inner">
                        {getUserSubgroupGrade(subgroup.gradeType, subgroup.tasks, subgroup.modules) ?? '-'}
                      </div>
                    </Tooltip>
                    <Tooltip title="Ваша оцінка за підгрупу">
                      <div className="h-full bg-gray-700 px-3 w-20 text-center text-gray-300 rounded shadow-inner">
                        {subgroup.grades[0]?.grade || '-'}
                      </div>
                    </Tooltip>
                    </>
                  )}
                </div>
                </>
              }
            >
              <p className="text-gray-300 text-xs">Спосіб вирахування оцінки: <span className="text-gray-50">{subgroup.gradeType === GradeCalculationType.AVERAGE ? 'Середнє арифметичне всіх оцінок' : (subgroup.gradeType === GradeCalculationType.SUM && 'Сума всіх оцінок')}</span></p>
              <div className="bg-gray-700 rounded p-2 text-gray-300 space-y-3">
                {subgroup.modules && <ModulesList onChange={onChange} modules={subgroup.modules} subgroupProp={subgroup} />}
                <TasksList dontDisplayEmpty={subgroup.modules.length > 0} tasks={subgroup.tasks} isTaskOfSubGroup subgroup={subgroup} onChange={onChange} />
              </div>
            </StyledPanel>
          );
        })}
      </Collapse>
    )}
    <Modal
      title={`Нове завдання в підгрупі '${selectedSubgroupForModal?.name}' групи '${allGroups?.find(g => g.subgroups.find(sg => sg._id === selectedSubgroupForModal?._id))?.name}'`}
      centered
      footer={null}
      visible={createTaskModalVisible}
      onCancel={() => setCreateTaskModalVisible(false)}
    >
      <CreateTaskForm isTaskOfSubGroup onTaskCreated={() => {
        setCreateTaskModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      title={`Новий модуль в підгрупі '${selectedSubgroupForModal?.name}' групи '${allGroups?.find(g => g.subgroups.find(sg => sg._id === selectedSubgroupForModal?._id))?.name}'`}
      centered
      footer={null}
      visible={createModuleModalVisible}
      onCancel={() => setCreateModuleModalVisible(false)}
    >
      <CreateModuleForm onModuleCreated={() => {
        setCreateModuleModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      title="Редагувати підгрупу"
      centered
      footer={null}
      visible={editSubgroupModalVisible}
      onCancel={() => setEditSubgroupModalVisible(false)}
    >
      <CreateSubgroupForm groupProp={groupProp} editMode onSubgroupCreated={() => {
        setEditSubgroupModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      width={620}
      title="Оцінки"
      centered
      footer={null}
      visible={gradesModalVisible}
      onCancel={() => {
        setGradesModalVisible(false);
        updateAllTasks();
        // updateCourse();
        // dispatch(deleteSelectedTaskForGrades());
      }}
    >
      <SubgroupGrades />
    </Modal>
    </>
  );
}
