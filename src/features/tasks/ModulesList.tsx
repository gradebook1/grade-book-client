import { PlusCircleFilled, SettingFilled } from '@ant-design/icons';
import { Collapse, Button, Modal, Tooltip } from 'antd';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { IModulePopulated } from '../../api/modules.api';
import { ISubgroupPopulated } from '../../api/subgroups.api';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectUserCourseRole, UserCourseRole } from '../../app/courseSlice';
import { selectGroups, selectSelectedModuleForModal, setSelectedModuleForGrades, setSelectedModuleForModal } from '../../app/tasksSlice';
import { IconButton } from '../../components/FixedIconButton';
import useUpdateAllTasks from '../../hooks/updateAllTasks.hook';
import useUpdateCourseInfo from '../../hooks/updateCourseInfo.hook';
import { getUserModuleGrade } from '../../services/getCourseGrade';
import CreateModuleForm from '../createModuleForm/createModuleForm';
import CreateTaskForm from '../createTaskForm/createTaskForm';
import ModuleGrades from '../grades/ModuleGrades';
import TasksList from './TasksList';

const { Panel } = Collapse;

const StyledPanel = styled(Panel)`
  & .ant-collapse-arrow {
    color: white !important;
  }
`;

interface IModulesListProps {
  modules: IModulePopulated[];
  onChange?: () => any;
  subgroupProp?: ISubgroupPopulated;
}

export default function ModulesList({ modules, onChange, subgroupProp } : IModulesListProps) {
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  const userCourseRole = useSelector(selectUserCourseRole);
  const selectedModuleForModal = useSelector(selectSelectedModuleForModal);
  const allGroups = useSelector(selectGroups);

  const [createTaskModalVisible, setCreateTaskModalVisible] = useState(false);

  const [editModuleModalVisible, setEditModuleModalVisible] = useState(false);

  const [gradesModalVisible, setGradesModalVisible] = useState(false);

  const [updateAllTasks] = useUpdateAllTasks();
  const [updateCourse] = useUpdateCourseInfo();

  const handleCreateTask = (event: any, module: IModulePopulated) => {
    event.stopPropagation();
    dispatch(setSelectedModuleForModal(module));
    setCreateTaskModalVisible(true);
  }

  const handleGradesClick = (event: any, module: IModulePopulated) => {
    event.stopPropagation();
    dispatch(setSelectedModuleForGrades(module));
    setGradesModalVisible(true);
  };

  const handleEdit = (event: any, module: IModulePopulated) => {
    event.stopPropagation();
    dispatch(setSelectedModuleForModal(module));
    setEditModuleModalVisible(true);
  };

  return (
    <>
    {modules.length > 0 && (
      <Collapse ghost className="space-y-3 rounded border-none shadow-lg">
        {modules.map(module => {
          return (
            <StyledPanel
              className="bg-gray-800 rounded"
              key={module._id}
              header={
                <>
                <span className="text-gray-400 text-xs">Модуль </span>
                <span className="text-gray-50">{module.name}</span>
                </>
              }
              extra={
                <>
                <div className="flex ml-auto space-x-4">
                  {userCourseRole === UserCourseRole.COURSE_TEACHER && (
                    <>
                    <Button size="small" shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={event => handleCreateTask(event, module)}>Створити завдання</Button>
                    <Button type="primary" size="small" onClick={event => handleGradesClick(event, module)}>Оцінки</Button>
                    <Tooltip title="Редагувати модуль">
                      <IconButton ghost type="link" size="small" shape="circle" icon={<SettingFilled />} onClick={event => handleEdit(event, module)} />
                    </Tooltip>
                    </>
                  )}
                  {userCourseRole === UserCourseRole.STUDENT && (
                    <>
                    <Tooltip title="Вирахувана оцінка за модуль">
                      <div className="h-full bg-gray-700 px-3 w-20 text-center text-gray-300 rounded shadow-inner">
                        {getUserModuleGrade(module.gradeType, module.tasks) ?? '-'}
                      </div>
                    </Tooltip>
                    <Tooltip title="Ваша оцінка за модуль">
                      <div className="h-full bg-gray-700 px-3 w-20 text-center text-gray-300 rounded shadow-inner">
                        {module.grades[0]?.grade || '-'}
                      </div>
                    </Tooltip>
                    </>
                  )}
                </div>
                </>
              }
            >
              <p className="text-gray-300 text-xs">Спосіб вирахування оцінки: <span className="text-gray-50">{module.gradeType === GradeCalculationType.AVERAGE ? 'Середнє арифметичне всіх оцінок' : (module.gradeType === GradeCalculationType.SUM && 'Сума всіх оцінок')}</span></p>
              <div className="bg-gray-700 rounded p-2 text-gray-300">
                <TasksList tasks={module.tasks} isTaskOfModule module={module} onChange={onChange} />
              </div>
            </StyledPanel>
          );
        })}
      </Collapse>
    )}
    <Modal
      title={`Нове завдання в модулі '${selectedModuleForModal?.name}' підгрупи '${allGroups?.find(g => g.subgroups.find(sg => sg.modules.find(m => m._id === selectedModuleForModal?._id)))?.subgroups.find(sg => sg.modules.find(m => m._id === selectedModuleForModal?._id))?.name}' групи '${allGroups?.find(g => g.subgroups.find(sg => sg.modules.find(m => m._id === selectedModuleForModal?._id)))?.name}'`}
      centered
      footer={null}
      visible={createTaskModalVisible}
      onCancel={() => setCreateTaskModalVisible(false)}
    >
      <CreateTaskForm isTaskOfModule onTaskCreated={() => {
        setCreateTaskModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      title="Редагувати модуль"
      centered
      footer={null}
      visible={editModuleModalVisible}
      onCancel={() => setEditModuleModalVisible(false)}
    >
      <CreateModuleForm subgroupProp={subgroupProp} editMode onModuleCreated={() => {
        setEditModuleModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      width={620}
      title="Оцінки"
      centered
      footer={null}
      visible={gradesModalVisible}
      onCancel={() => {
        setGradesModalVisible(false);
        updateAllTasks();
        // updateCourse();
        // dispatch(deleteSelectedTaskForGrades());
      }}
    >
      <ModuleGrades />
    </Modal>
    </>
  );
}
