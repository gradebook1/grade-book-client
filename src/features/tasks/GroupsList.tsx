import { PlusCircleFilled, SettingFilled } from '@ant-design/icons';
import { Collapse, Button, Modal, Tooltip } from 'antd';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { IGroupPopulated } from '../../api/groups.api';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectUserCourseRole, UserCourseRole } from '../../app/courseSlice';
import { selectSelectedGroupForModal, setSelectedGroupForGrades, setSelectedGroupForModal } from '../../app/tasksSlice';
import { IconButton } from '../../components/FixedIconButton';
import useUpdateAllTasks from '../../hooks/updateAllTasks.hook';
import useUpdateCourseInfo from '../../hooks/updateCourseInfo.hook';
import { getUserGroupGrade } from '../../services/getCourseGrade';
import CreateGroupForm from '../createGroupForm/createGroupForm';
import CreateSubgroupForm from '../createSubgroupForm/createSubgroupForm';
import CreateTaskForm from '../createTaskForm/createTaskForm';
import GroupGrades from '../grades/GroupGrades';
import SubgroupsList from './SubgroupsList';
import TasksList from './TasksList';

const { Panel } = Collapse;

const StyledPanel = styled(Panel)`
  & .ant-collapse-arrow {
    color: white !important;
  }
`;

interface IGroupsListProps {
  groups: IGroupPopulated[];
  onChange?: () => any;
}

export default function GroupsList({ groups, onChange } : IGroupsListProps) {
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  const userCourseRole = useSelector(selectUserCourseRole);
  const selectedGroupForModal = useSelector(selectSelectedGroupForModal);

  const [createTaskModalVisible, setCreateTaskModalVisible] = useState(false);
  const [createSubgroupModalVisible, setCreateSubgroupModalVisible] = useState(false);

  const [editGroupModalVisible, setEditGroupModalVisible] = useState(false);

  const [gradesModalVisible, setGradesModalVisible] = useState(false);

  const [updateAllTasks] = useUpdateAllTasks();
  const [updateCourse] = useUpdateCourseInfo();

  const handleCreateTask = (event: any, group: IGroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedGroupForModal(group));
    setCreateTaskModalVisible(true);
  }

  const handleCreateSubgroup = (event: any, group: IGroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedGroupForModal(group));
    setCreateSubgroupModalVisible(true);
  }

  const handleGradesClick = (event: any, group: IGroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedGroupForGrades(group));
    setGradesModalVisible(true);
  };

  const handleEdit = (event: any, group: IGroupPopulated) => {
    event.stopPropagation();
    dispatch(setSelectedGroupForModal(group));
    setEditGroupModalVisible(true);
  };

  return (
    <>
    {groups.length > 0 && (
      <Collapse ghost className="space-y-3 rounded border-none shadow-lg">
        {groups.map(group => {
          return (
            <StyledPanel
              className="bg-gray-800 rounded"
              key={group._id}
              header={
                <>
                <span className="text-gray-400 text-xs">Група </span>
                <span className="text-gray-50">{group.name}</span>
                </>
              }
              extra={
                <>
                <div className="flex ml-auto space-x-4">
                  {userCourseRole === UserCourseRole.COURSE_TEACHER && (
                    <>
                    <Button size="small" shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={event => handleCreateTask(event, group)}>Створити завдання</Button>
                    <Button size="small" shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={event => handleCreateSubgroup(event, group)}>Створити підгрупу</Button>
                    <Button type="primary" size="small" onClick={event => handleGradesClick(event, group)}>Оцінки</Button>
                    <Tooltip title="Редагувати групу">
                      <IconButton ghost type="link" size="small" shape="circle" icon={<SettingFilled />} onClick={event => handleEdit(event, group)} />
                    </Tooltip>
                    </>
                  )}
                  {userCourseRole === UserCourseRole.STUDENT && (
                    <>
                    <Tooltip title="Вирахувана оцінка за групу">
                      <div className="h-full bg-gray-700 px-3 w-20 text-center text-gray-300 rounded shadow-inner">
                        {getUserGroupGrade(group.gradeType, group.tasks, group.subgroups) ?? '-'}
                      </div>
                    </Tooltip>
                    <Tooltip title="Ваша оцінка за групу">
                      <div className="h-full bg-gray-700 px-3 w-20 text-center text-gray-300 rounded shadow-inner">
                        {group.grades[0]?.grade || '-'}
                      </div>
                    </Tooltip>
                    </>
                  )}
                </div>
                </>
              }
            >
              <p className="text-gray-300 text-xs">Спосіб вирахування оцінки: <span className="text-gray-50">{group.gradeType === GradeCalculationType.AVERAGE ? 'Середнє арифметичне всіх оцінок' : (group.gradeType === GradeCalculationType.SUM && 'Сума всіх оцінок')}</span></p>
              <div className="bg-gray-700 rounded p-2 text-gray-300 space-y-3">
                {group.subgroups && <SubgroupsList onChange={onChange} subgroups={group.subgroups} groupProp={group} />}
                <TasksList dontDisplayEmpty={group.subgroups.length > 0} tasks={group.tasks} isTaskOfGroup group={group} onChange={onChange} />
              </div>
            </StyledPanel>
          );
        })}
      </Collapse>
    )}
    <Modal
      title={`Нове завдання в групі ${selectedGroupForModal?.name}`}
      centered
      footer={null}
      visible={createTaskModalVisible}
      onCancel={() => setCreateTaskModalVisible(false)}
    >
      <CreateTaskForm isTaskOfGroup onTaskCreated={() => {
        setCreateTaskModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      title="Нова підгрупа"
      centered
      footer={null}
      visible={createSubgroupModalVisible}
      onCancel={() => setCreateSubgroupModalVisible(false)}
    >
      <CreateSubgroupForm onSubgroupCreated={() => {
        setCreateSubgroupModalVisible(false);
        if (onChange) onChange();
      }} />
    </Modal>
    <Modal
      title="Редагувати групу"
      centered
      footer={null}
      visible={editGroupModalVisible}
      onCancel={() => setEditGroupModalVisible(false)}
    >
      <CreateGroupForm editMode onGroupCreated={() => {
        setEditGroupModalVisible(false);
        if (onChange) onChange();
        // loadClassroomInfo();
        // history.push(match.url + `/c/${courseInfo?.urlCode}`);
        // TODO: Reload all tasks and groups here. Should be silent (without loader displaying) !!!!!!!
      }} />
    </Modal>
    <Modal
      width={620}
      title="Оцінки"
      centered
      footer={null}
      visible={gradesModalVisible}
      onCancel={() => {
        setGradesModalVisible(false);
        updateAllTasks();
        // updateCourse();
        // dispatch(deleteSelectedTaskForGrades());
      }}
    >
      <GroupGrades />
    </Modal>
    </>
  );
}
