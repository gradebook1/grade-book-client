import { PlusCircleFilled } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams, useRouteMatch } from 'react-router-dom';
import TasksAPI from '../../api/tasks.api';
import { selectUserCourseRole, UserCourseRole } from '../../app/courseSlice';
import { selectGroups, selectTasks, setTasks } from '../../app/tasksSlice';
import Loader from '../../components/Loader/Loader';
import CreateGroupForm from '../createGroupForm/createGroupForm';
import CreateTaskForm from '../createTaskForm/createTaskForm';
import GroupsList from './GroupsList';
import TasksList from './TasksList';

export default function TasksBlock() {
  const userCourseRole = useSelector(selectUserCourseRole);
  const courseLevelTasks = useSelector(selectTasks);
  const groups = useSelector(selectGroups);

  const [createTaskModalVisible, setCreateTaskModalVisible] = useState(false);
  const [createGroupModalVisible, setCreateGroupModalVisible] = useState(false);

  const [isLoading, setLoading] = useState(true);
  const [isError, setError] = useState(false);
  const { classroomUrlCode, courseUrlCode } = useParams<{ classroomUrlCode: string, courseUrlCode: string }>();

  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  const loadAllTasks = async () => {
    // setLoading(true);
    setError(false);
    const { data, error } = await TasksAPI.getAllTasks(courseUrlCode, classroomUrlCode);
    if (data) {
      dispatch(setTasks(data));
      setLoading(false);
    } else if (error) {
      if (error.response && error.response.data.message === 'no classroom') {
        history.push('/app');
      } else if (error.response && error.response.data.message === 'no course') {
        history.push(`/app/${classroomUrlCode}`);
      }
      // TODO: Handle here 403 forbidden error to show that user doesnt have access to that private course
      setError(true);
      setLoading(false);
    }
  }

  useEffect(() => {
    loadAllTasks();
  }, []);

  let content;
  if (isLoading) {
    content = <Loader />;
  } else if (isError) {
    content = <h2 className="text-gray-300 text-center">Помилка при завантажені даних. Спробуйте пізніше.</h2>
  } else {
    content = (
      <>
      <div id="groups">
        {groups && <GroupsList onChange={() => loadAllTasks()} groups={groups} />}
      </div>
      <div id="tasks" className="pt-3">
        {courseLevelTasks && <TasksList dontDisplayEmpty={(groups && groups.length > 0) ?? false} onChange={() => loadAllTasks()} tasks={courseLevelTasks} />}
      </div>
      </>
    );
  }

  // if (selectedTaskForGrades) {
  //   return (
  //     <>
  //     <div>
  //       <h3 className="text-center text-gray-50 text-lg">{selectedTaskForGrades.name}</h3>
  //     </div>
  //     <TaskGrades task={selectedTaskForGrades} />
  //     </>
  //   );
  // }

  return (
    <>
    <div>
      <h3 className="text-center text-gray-50 text-lg">Завдання</h3>
      <div className="rounded px-5 py-2 flex">
        <div className="flex pb-2 space-x-4">
          {userCourseRole === UserCourseRole.COURSE_TEACHER && (
            <>
            <Button shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={() => setCreateTaskModalVisible(true)}>Створити завдання</Button>
            <Button shape="round" type="dashed" ghost icon={<PlusCircleFilled />} className="flex justify-center items-center" onClick={() => setCreateGroupModalVisible(true)}>Створити групу</Button>
            </>
          )}
        </div>
      </div>
      {content}
    </div>
    <Modal
      title="Нове завдання"
      centered
      footer={null}
      visible={createTaskModalVisible}
      onCancel={() => setCreateTaskModalVisible(false)}
    >
      <CreateTaskForm onTaskCreated={() => {
        setCreateTaskModalVisible(false);
        loadAllTasks();
      }} />
    </Modal>
    <Modal
      title="Нова група"
      centered
      footer={null}
      visible={createGroupModalVisible}
      onCancel={() => setCreateGroupModalVisible(false)}
    >
      <CreateGroupForm onGroupCreated={() => {
        setCreateGroupModalVisible(false);
        loadAllTasks();
      }} />
    </Modal>
    </>
  );
}
