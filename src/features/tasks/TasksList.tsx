import { Empty, Modal } from 'antd';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { IGroupPopulated } from '../../api/groups.api';
import { IModulePopulated } from '../../api/modules.api';
import { ISubgroupPopulated } from '../../api/subgroups.api';
import { ITask } from '../../api/tasks.api';
import { deleteSelectedTaskForGrades, setSelectedTaskForGrades, setSelectedTaskForModal } from '../../app/tasksSlice';
import useUpdateAllTasks from '../../hooks/updateAllTasks.hook';
import useUpdateCourseInfo from '../../hooks/updateCourseInfo.hook';
import CreateTaskForm from '../createTaskForm/createTaskForm';
import Task from './Task';
import TaskGrades from './TaskGrades';

interface ITasksListProps {
  tasks: ITask[];
  dontDisplayEmpty?: boolean;
  onChange?: () => any;
  //
  isTaskOfGroup?: boolean;
  isTaskOfSubGroup?: boolean;
  isTaskOfModule?: boolean;
  group?: IGroupPopulated;
  subgroup?: ISubgroupPopulated;
  module?: IModulePopulated;
}

export default function TasksList({
  tasks,
  dontDisplayEmpty,
  onChange,
  isTaskOfGroup,
  isTaskOfSubGroup,
  isTaskOfModule,
  group,
  subgroup,
  module,
} : ITasksListProps) {
  const dispatch = useDispatch();
  const [editTaskModalVisible, setEditTaskModalVisible] = useState(false);
  const [taskGradesModalVisible, setTaskGradesModalVisible] = useState(false);

  const [updateAllTasks] = useUpdateAllTasks();
  const [updateCourse] = useUpdateCourseInfo();

  const handleEdit = (event: any, task: ITask) => {
    event.stopPropagation();
    dispatch(setSelectedTaskForModal(task));
    setEditTaskModalVisible(true);
  };

  const handleGradesOpen = (event: any, task: ITask) => {
    event.stopPropagation();
    dispatch(setSelectedTaskForGrades(task));
    setTaskGradesModalVisible(true);
  };

  return (
    <>
    <div className="mx-auto rounded space-y-3">
      {tasks.length === 0 && !dontDisplayEmpty ? <Empty description="Завдання відсутні"/> : (
        tasks.map(task => <Task key={task._id} task={task} onEditClicked={handleEdit} onGradesClicked={handleGradesOpen} />)
      )}
    </div>
    <Modal
      title="Редагувати завдання"
      centered
      footer={null}
      visible={editTaskModalVisible}
      onCancel={() => setEditTaskModalVisible(false)}
    >
      <CreateTaskForm
        editMode
        isTaskOfGroup={isTaskOfGroup}
        isTaskOfModule={isTaskOfModule}
        isTaskOfSubGroup={isTaskOfSubGroup}
        group={group}
        subgroup={subgroup}
        module={module}
        onTaskCreated={() => {
        setEditTaskModalVisible(false);
          if (onChange) onChange();
        }}
      />
    </Modal>
    <Modal
      title="Оцінки"
      centered
      footer={null}
      visible={taskGradesModalVisible}
      onCancel={() => {
        setTaskGradesModalVisible(false);
        updateAllTasks();
        // updateCourse();
        // dispatch(deleteSelectedTaskForGrades());
      }}
    >
      <TaskGrades />
    </Modal>
    </>
  );
}
