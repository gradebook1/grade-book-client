import { SettingFilled } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { ITask } from '../../api/tasks.api';
import { selectUserCourseRole, UserCourseRole } from '../../app/courseSlice';
import { setSelectedTaskForGrades } from '../../app/tasksSlice';
import { IconButton } from '../../components/FixedIconButton';
import PTag from '../../components/Tag';

interface ITaskProps {
  task: ITask;
  onEditClicked?: (event: any, task: ITask) => any;
  onGradesClicked?: (event: any, task: ITask) => any;
}

export default function Task({ task, onEditClicked, onGradesClicked } : ITaskProps) {
  const userCourseRole = useSelector(selectUserCourseRole);

  const match = useRouteMatch();
  const history = useHistory();
  const dispatch = useDispatch();

  // const handleGradesClick = () => {
  //   dispatch(setSelectedTaskForGrades(task));
  //   history.push(`${match.url}/tg`);
  // };
  
  return (
    <div className="flex justify-between rounded p-2 shadow-lg bg-gray-800">
      <div className="flex space-x-2">
        <h2 className="text-gray-50 mb-0">{task.name}</h2>
        {task.isPrivate && <PTag tooltip="Приватне завдання">П</PTag>}
      </div>
      <span>{new Date(task.date).toLocaleString()}</span>
      {userCourseRole === UserCourseRole.COURSE_TEACHER && (
        <div id="task-options" className="flex space-x-4">
          <Button type="primary" size="small" onClick={event => onGradesClicked && onGradesClicked(event, task)}>Оцінки</Button>
          <Tooltip title="Редагувати завдання">
            <IconButton ghost type="link" size="small" shape="circle" icon={<SettingFilled />} onClick={event => onEditClicked && onEditClicked(event, task)} />
          </Tooltip>
        </div>
      )}
      {userCourseRole === UserCourseRole.STUDENT && (
        <Tooltip title="Ваша оцінка">
          <div className="bg-gray-700 px-3 w-20 text-center rounded shadow-inner">
            {task.grades[0]?.grade || '-'}
          </div>
        </Tooltip>
      )}
    </div>
  );
}
