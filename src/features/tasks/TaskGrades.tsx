import { message } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import TasksAPI from '../../api/tasks.api';
import { selectClassroom } from '../../app/classroomSlice';
import { selectCourse } from '../../app/courseSlice';
import { deleteSelectedTaskForGrades, selectSelectedTaskForGrades } from '../../app/tasksSlice';
import { DarkLoader } from '../../components/Loader/Loader';
import PTag from '../../components/Tag';
import UsersGradesTable from '../../components/UsersGradesTable/UsersGradesTable';

export default function TaskGrades() {
  const dispatch = useDispatch();
  const history = useHistory();
  const selectedTaskForGrades = useSelector(selectSelectedTaskForGrades);
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);

  useEffect(() => {
    return () => {
      if (selectedTaskForGrades) dispatch(deleteSelectedTaskForGrades());
    }
  }, []);

  // if (!selectedTaskForGrades) {
  //   return <DarkLoader />
  // }

  // NOTE: Calculate task's users
  // const users = selectedTaskForGrades?.isPrivate ? selectedTaskForGrades.privateStudents : (
  //   courseInfo?.isPrivate ? courseInfo.privateStudents : classroomInfo?.students
  // );

  const users = courseInfo?.isPrivate ? (
    selectedTaskForGrades?.isPrivate ? selectedTaskForGrades.privateStudents?.filter(student => courseInfo.privateStudents?.find(st => st._id === student._id)) : courseInfo.privateStudents
  ) : (
    selectedTaskForGrades?.isPrivate ? selectedTaskForGrades.privateStudents : classroomInfo?.students
  );

  const saveGrade = async (userId: string, grade?: number) => {
    const { error } = await TasksAPI.updateCourseLevelTaskGrade({
      course: courseInfo?._id || '',
      task: selectedTaskForGrades?._id || '',
      student: userId,
      grade: grade,
    });
    
    if (error) {
      message.error('Виникла помилка при оновленні оцінки! Спробуйте пізніше.');
    }
  }

  return (
    <div>
      {/* TODO: Add button to go back to tasks */}
      <div className="flex justify-center items-center space-x-3 mb-3">
        <h3 className="text-center text-gray-700 text-lg mb-0">
          {selectedTaskForGrades?.name}
        </h3>
        {selectedTaskForGrades?.isPrivate && <PTag tooltip="Приватне завдання">П</PTag>}
      </div>
      <UsersGradesTable users={users} grades={selectedTaskForGrades?.grades} onGradeChange={saveGrade}/>
    </div>
  );
}
