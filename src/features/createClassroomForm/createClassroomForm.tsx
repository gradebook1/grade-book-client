import React, { useEffect } from 'react';
import { Form, Input, Button, Modal, message } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import ClassroomsAPI from '../../api/classrooms.api';
import { useSelector } from 'react-redux';
import { selectClassroom } from '../../app/classroomSlice';
import { useHistory } from 'react-router-dom';

function showEditPromiseConfirm(classroomId: string, values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await ClassroomsAPI.updateClassroom(classroomId, values);
        if (data) {
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Створити новий клас?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть створення нового класу.',
    onOk() {
      async function request() {
        const { data, error } = await ClassroomsAPI.createClassroom(values);
        if (data) {
          afterCreation();
          // Show message about successful creation
          message.success('Ви успішно створили новий клас!');
        } else if (error) {
          // TODO: Show message about error
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

interface ICreateClassroomFormProps {
  onClassroomCreated: () => any;
  editMode?: boolean;
  className?: string;
}

export default function CreateClassroomForm({ onClassroomCreated, editMode, className }:ICreateClassroomFormProps) {
  const [form] = Form.useForm();
  const classroomInfo = useSelector(selectClassroom);
  const history = useHistory();

  if (!classroomInfo && editMode) {
    history.push('/app');
  }

  const afterCreation = () => {
    if (!editMode) {
      form.resetFields();
    }
    onClassroomCreated();
  };

  const onFinish = async (values: any) => {
    if (editMode) {
      showEditPromiseConfirm(classroomInfo?._id || '', values, afterCreation);
    } else {
      showPromiseConfirm(values, afterCreation);
    }
  };

  useEffect(() => {
    if (!editMode) return;

    form.setFieldsValue({
      name: classroomInfo?.name,
      description: classroomInfo?.description,
    });
  }, [editMode, classroomInfo]);

  return (
    <Form
      form={form}
      name="createClassroom"
      onFinish={onFinish}
      className={className}
    >
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Введіть назву класу!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Назва класу"
        />
      </Form.Item>
      <Form.Item
        name="description"
        rules={[
          { required: true, message: 'Введіть опис!' },
          { max: 100, message: 'Максимум 100 символів!' }
        ]}
      >
        <Input.TextArea
          showCount
          maxLength={100}
          placeholder="Короткий опис"
        />
      </Form.Item>
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit">
          {editMode ? 'Зберегти зміни' : 'Створити клас'}
        </Button>
      </Form.Item>
    </Form>
  );
}