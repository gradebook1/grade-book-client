import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Modal, message, Select, Switch, DatePicker } from 'antd';
import { ExclamationCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectClassroom } from '../../app/classroomSlice';
import { useHistory } from 'react-router';
import SelectUsersTable from '../../components/SelectUsersTable/SelectUsersTable';
import { selectCourse } from '../../app/courseSlice';
import TasksAPI, { IDeleteTaskDTO } from '../../api/tasks.api';
import GroupsAPI, { IGroupPopulated } from '../../api/groups.api';
import { selectSelectedGroupForModal, selectSelectedModuleForModal, selectSelectedSubgroupForModal, selectSelectedTaskForModal } from '../../app/tasksSlice';
import SubgroupsAPI, { ISubgroupPopulated } from '../../api/subgroups.api';
import ModulesAPI, { IModulePopulated } from '../../api/modules.api';
import moment from 'moment';

const { Option } = Select;

enum TaskType {
  COURSE_TASK = 'course_task',
  GROUP_TASK = 'group_task',
  SUBGROUP_TASK = 'subgroup_task',
  MODULE_TASK = 'module_task',
}

function showEditPromiseConfirm(taskId: string, values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await TasksAPI.updateTask(taskId, values);
        if (data) {
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showPromiseConfirm(values: any, afterCreation: () => any, taskType: TaskType, entityId?: string) {
  Modal.confirm({
    title: 'Створити нове завдання?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть створення нового завдання.',
    onOk() {
      async function request() {
        let response;
        switch (taskType) {
          case TaskType.COURSE_TASK:
            response = await TasksAPI.createCourseLevelTask(values);
            break;
          case TaskType.GROUP_TASK:
            response = await GroupsAPI.createTask(entityId ?? '', values);
            break;
          case TaskType.SUBGROUP_TASK:
            response = await SubgroupsAPI.createTask(entityId ?? '', values);
            break;
          case TaskType.MODULE_TASK:
            response = await ModulesAPI.createTask(entityId ?? '', values);
            break;
          default:
            response = await TasksAPI.createCourseLevelTask(values);
            break;
        }
        const { data, error } = response;
        if (data) {
          afterCreation();
          message.success('Ви успішно створили нове завдання!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showDeletePromiseConfirm(deleteTaskDto: IDeleteTaskDTO, afterCreation: () => any, taskType: TaskType, entityId?: string) {
  Modal.confirm({
    title: 'Підтвердження видалення',
    icon: <WarningOutlined />,
    content: 'Підтвердіть видалення.',
    onOk() {
      async function request() {
        let response;
        switch (taskType) {
          case TaskType.COURSE_TASK:
            response = await TasksAPI.deleteCourseLevelTask(deleteTaskDto);
            break;
          case TaskType.GROUP_TASK:
            response = await GroupsAPI.deleteTask(entityId ?? '', deleteTaskDto);
            break;
          case TaskType.SUBGROUP_TASK:
            response = await SubgroupsAPI.deleteTask(entityId ?? '', deleteTaskDto);
            break;
          case TaskType.MODULE_TASK:
            response = await ModulesAPI.deleteTask(entityId ?? '', deleteTaskDto);
            break;
          default:
            response = await TasksAPI.deleteCourseLevelTask(deleteTaskDto);
            break;
        }
        const { data, error } = response;
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили завдання!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

// function showGroupTaskPromiseConfirm(values: any, afterCreation: () => any, groupId: string) {
//   Modal.confirm({
//     title: 'Створити нове завдання?',
//     icon: <ExclamationCircleOutlined />,
//     content: 'Підтвердіть створення нового завдання.',
//     onOk() {
//       async function request() {
//         const { data, error } = await GroupsAPI.createTask(groupId, values);
//         if (data) {
//           afterCreation();
//           message.success('Ви успішно створили нове завдання!');
//         } else if (error) {
//           message.error('Виникла помилка! Спробуйте пізніше.');
//         }
//       }

//       return request();
//     },
//     onCancel() {},
//     centered: true,
//     cancelText: 'Ні',
//     okText: 'Так',
//   });
// }

// function showSubgroupTaskPromiseConfirm(values: any, afterCreation: () => any, subgroupId: string) {
//   Modal.confirm({
//     title: 'Створити нове завдання?',
//     icon: <ExclamationCircleOutlined />,
//     content: 'Підтвердіть створення нового завдання.',
//     onOk() {
//       async function request() {
//         const { data, error } = await SubgroupsAPI.createTask(subgroupId, values);
//         if (data) {
//           afterCreation();
//           message.success('Ви успішно створили нове завдання!');
//         } else if (error) {
//           message.error('Виникла помилка! Спробуйте пізніше.');
//         }
//       }

//       return request();
//     },
//     onCancel() {},
//     centered: true,
//     cancelText: 'Ні',
//     okText: 'Так',
//   });
// }

interface ICreateTaskFormProps {
  editMode?: boolean;
  onTaskCreated: () => any;
  isTaskOfGroup?: boolean;
  isTaskOfSubGroup?: boolean;
  isTaskOfModule?: boolean;
  group?: IGroupPopulated;
  subgroup?: ISubgroupPopulated;
  module?: IModulePopulated;
}

export default function CreateTaskForm({
  editMode,
  onTaskCreated,
  isTaskOfGroup,
  isTaskOfSubGroup,
  isTaskOfModule,
  group,
  subgroup,
  module,
}:ICreateTaskFormProps) {
  const [form] = Form.useForm();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const selectedGroupForModal = useSelector(selectSelectedGroupForModal);
  const selectedSubgroupForModal = useSelector(selectSelectedSubgroupForModal);
  const selectedModuleForModal = useSelector(selectSelectedModuleForModal);
  const history = useHistory();
  const [isTableVisible, setTableVisible] = useState(false);
  const [editingStarted, setEditingStarted] = useState(false);
  const [r, rerender] = useState(false);

  const task = useSelector(selectSelectedTaskForModal);

  if (!classroomInfo || !courseInfo) {
    history.push('/app');
  }

  const afterCreation = () => {
    if (!editMode) {
      form.resetFields();
    }
    onTaskCreated();
  };

  const onFinish = async (values: any) => {
    values.course = courseInfo?._id;
    if (values.isPrivate === undefined) {
      values.isPrivate = false;
    }
    console.log(values);
    if (editMode) {
      showEditPromiseConfirm(task?._id || '', values, afterCreation);
      return;
    }
    if (isTaskOfGroup) {
      showPromiseConfirm(values, afterCreation, TaskType.GROUP_TASK, selectedGroupForModal?._id || '');
    } else if (isTaskOfSubGroup) {
      showPromiseConfirm(values, afterCreation, TaskType.SUBGROUP_TASK, selectedSubgroupForModal?._id || '');
    } else if (isTaskOfModule) {
      showPromiseConfirm(values, afterCreation, TaskType.MODULE_TASK, selectedModuleForModal?._id || '');
    } else {
      showPromiseConfirm(values, afterCreation, TaskType.COURSE_TASK);
    }
  };

  const handleDelete = () => {
    if (isTaskOfGroup) {
      showDeletePromiseConfirm({ course: courseInfo?._id || '', task: task?._id || '' }, afterCreation, TaskType.GROUP_TASK, selectedGroupForModal?._id || group?._id || '');
    } else if (isTaskOfSubGroup) {
      showDeletePromiseConfirm({ course: courseInfo?._id || '', task: task?._id || '' }, afterCreation, TaskType.SUBGROUP_TASK, selectedSubgroupForModal?._id || subgroup?._id || '');
    } else if (isTaskOfModule) {
      showDeletePromiseConfirm({ course: courseInfo?._id || '', task: task?._id || '' }, afterCreation, TaskType.MODULE_TASK, selectedModuleForModal?._id || module?._id || '');
    } else {
      showDeletePromiseConfirm({ course: courseInfo?._id || '', task: task?._id || '' }, afterCreation, TaskType.COURSE_TASK);
    }
  }

  useEffect(() => {
    if (!editMode) return;

    form.setFieldsValue({
      name: task?.name,
      date: moment(task?.date),
      isPrivate: task?.isPrivate,
      privateStudents: task?.privateStudents?.map(user => user._id),
    });

    if (editMode && task?.isPrivate) {
      setTableVisible(true);
    } else {
      setTableVisible(false);
    }
  }, [editMode, task]);

  return (
    <>
    <Form
      form={form}
      name="createTask"
      onFinish={onFinish}
    >
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Введіть назву завдання!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Назва завдання"
        />
      </Form.Item>
      <Form.Item name="course" hidden>
        <Input type="text" value={courseInfo?._id}></Input>
      </Form.Item>
      <Form.Item name="date" rules={[
        { required: true, message: 'Виберіть дату!' },
      ]}>
        <DatePicker showTime />
      </Form.Item>
      <Form.Item label="Приватне завдання" name="isPrivate">
        {/* <Switch onChange={(checked) => {
          setTableVisible(checked);
          if (!checked) {
            form.setFieldsValue({ privateStudents: [] });
          }
        }} /> */}
        <Switch checked={editingStarted ? isTableVisible : (editMode && task?.isPrivate)} onChange={(checked) => {
          setTableVisible(checked);
          if (!checked) {
            form.setFieldsValue({ privateStudents: [] });
          }
          setEditingStarted(true);
        }} />
      </Form.Item>
      {isTableVisible && <Form.Item name="privateStudents">
        <SelectUsersTable
          selectionType="checkbox"
          users={courseInfo!.isPrivate ? courseInfo!.privateStudents! : classroomInfo!.students}
          selectedUsers={form.getFieldValue('privateStudents')}
          onChange={(usersKeys) => {
            form.setFieldsValue({privateStudents: usersKeys});
            rerender(!r);
          }}
        />
      </Form.Item>}
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit">
        {editMode ? 'Зберегти зміни' : 'Створити завдання'}
        </Button>
      </Form.Item>
    </Form>
    {editMode && (
      <Button danger type="primary" className="mt-3" onClick={handleDelete}>
        Видалити
      </Button>
    )}
    </>
  );
}