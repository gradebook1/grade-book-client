import React from 'react';
import { Form, Input, Button, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import AuthAPI from '../../api/auth.api';
import { useHistory } from 'react-router';
// import { useForm } from 'react-hook-form';

// export default function LoginForm() {
//   const { register, handleSubmit, errors } = useForm();
//   const onSubmit = (data: any) => console.log(data);
//   console.log(errors);
  
//   return (
//     <form onSubmit={handleSubmit(onSubmit)}>
//       <input type="email" placeholder="Email" name="email" ref={register({required: true, pattern: /^\S+@\S+$/i})} />
//       <input type="password" placeholder="Пароль" name="password" ref={register({required: true, pattern: /^[a-zA-Z0-9]$/i})} />

//       <input type="submit" />
//     </form>
//   );
// }

export default function LoginForm() {
  const history = useHistory();

  const onFinish = async (values: any) => {
    const { data, error } = await AuthAPI.login(values);
    if (data) {
      localStorage.setItem('userData', JSON.stringify(data));
      history.push('/app');
    } else if (error) {
      if (error.response && error.response.data.message === 'no user') {
        message.error('Користувача з такою електронною поштою не існує!');
      } else if (error.response && error.response.data.message === 'password') {
        message.error('Ви ввели неправильний пароль!');
      }
    }
  };

  return (
    <Form
      name="login"
      className="login-form"
      // initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          { required: true, message: 'Введіть свою електронну пошту!' },
          { pattern: /^\S+@\S+.\S+$/, message: 'Щось це не схоже на електронну пошту...' }
        ]}
      >
        <Input type="email" prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Електронна пошта" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          { required: true, message: 'Введіть пароль!' },
          { min: 6, message: 'Мінімум 6 символів!' }
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Пароль"
        />
      </Form.Item>
      {/* <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <a className="login-form-forgot" href="">
          Forgot password
        </a>
      </Form.Item> */}

      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit" className="w-full">
          Увійти
        </Button>
      </Form.Item>
    </Form>
  );
}