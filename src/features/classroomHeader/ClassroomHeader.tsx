import React from 'react';
import { PageHeader, Button, Descriptions } from 'antd';
import { useSelector } from 'react-redux';
import { selectClassroom, selectUserClassroomRole, UserClassroomRole } from '../../app/classroomSlice';
import styled from 'styled-components';
import { useHistory } from 'react-router';

export function ClassroomHeader() {
  const classroomInfo = useSelector(selectClassroom);
  const userClassroomRole = useSelector(selectUserClassroomRole);
  const history = useHistory();

  if (!classroomInfo || ! userClassroomRole) {
    history.push('/app');
  }

  const classButtons = [
    <Button
    key="1"
    type="primary"
    onClick={() => history.push(`/app/${classroomInfo?.urlCode}/users`)}
  >
    Користувачі
  </Button>,
  ];

  if (userClassroomRole === UserClassroomRole.CLASSHEAD) {
    classButtons.unshift(
      <Button
        key="3"
        onClick={() => history.push(`/app/${classroomInfo?.urlCode}/settings`)}
      >
        Налаштування класу
      </Button>
    );
  }

  return (
    <PageHeader
      className=" text-gray-300 rounded-lg"
      ghost={false}
      onBack={() => history.push('/app')}
      title={classroomInfo?.name}
      subTitle={classroomInfo?.description}
      extra={classButtons}
    >
      <Descriptions size="small" column={{ xxl: 4, xl: 3, lg: 2, md: 2, sm: 1, xs: 1 }}>
        <Descriptions.Item label="Керівник класу">
          {`${classroomInfo?.classhead.firstName} ${classroomInfo?.classhead.lastName}`}
        </Descriptions.Item>
        <Descriptions.Item label="Дата створення класу">
          {classroomInfo && new Date(classroomInfo.createdDate).toLocaleString()}
        </Descriptions.Item>
        <Descriptions.Item label="Кількість учнів">
          {classroomInfo?.students.length}
        </Descriptions.Item>
        <Descriptions.Item label="Кількість викладачів">
          {1 + classroomInfo!.teachers.length}
        </Descriptions.Item>
      </Descriptions>
    </PageHeader>
  );
} 

const StyledPageHeader = styled(PageHeader)`
  & span {
    --tw-text-opacity: 1;
    color: rgba(209, 213, 219, var(--tw-text-opacity));
  }
`;
