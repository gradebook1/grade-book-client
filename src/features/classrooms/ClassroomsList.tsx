import { PlusCircleFilled } from '@ant-design/icons';
import { Button, Empty, Modal } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useRouteMatch } from 'react-router-dom';
import ClassroomsAPI, { IClassroom, IClassroomsList } from '../../api/classrooms.api';
import Loader from '../../components/Loader/Loader';
import CreateClassroomForm from '../createClassroomForm/createClassroomForm';
import JoinClassroomForm from '../joinClassroomForm/JoinClassroomForm';

export default function ClassroomsBlock() {
  const [classrooms, setClassrooms] = useState<IClassroomsList>();
  const [isLoading, setLoading] = useState(true);
  const [isError, setError] = useState(false);

  const [createClassroomModalVisible, setCreateClassroomModalVisible] = useState(false);
  const [joinClassroomModalVisible, setJoinClassroomModalVisible] = useState(false);

  const loadClassrooms = async () => {
    const { data, error } = await ClassroomsAPI.getClassrooms();
    if (data) {
      setClassrooms(data);
      setLoading(false);
    } else if (error) {
      setError(true);
      setLoading(false);
    }
  }

  useEffect(() => {
    loadClassrooms();
  }, []);

  let content;
  if (isLoading) {
    content = <Loader />;
  } else if (isError) {
    content = <h2 className="text-gray-300">Помилка при завантажені даних. Спробуйте пізніше.</h2>
  } else if (classrooms) {
    content = (
      <>
      <ClassroomsList title="Створені мною класи" classrooms={classrooms.createdBy} />
      <ClassroomsList title="Класи, в яких я навчаюсь" classrooms={classrooms.whereStudent} />
      <ClassroomsList title="Класи, в яких я викладаю" classrooms={classrooms.whereTeacher} />
      </>
    );
  }

  return (
    <div className="pt-5 px-2 pb-5 max-w-3xl mx-auto space-y-5">
      <div className="flex justify-end px-5 text-xl space-x-3">
        <Button
          type="primary"
          className="rounded-full"
          onClick={() => setJoinClassroomModalVisible(true)}
        >
          Приєднатися до класу
        </Button>
        <Button
          type="dashed"
          ghost
          icon={<PlusCircleFilled />}
          className="rounded-full flex items-center"
          onClick={() => setCreateClassroomModalVisible(true)}
        >
          Створити клас
        </Button>
      </div>
      {content}
      <Modal
        title="Створити клас"
        centered
        footer={null}
        visible={createClassroomModalVisible}
        onCancel={() => setCreateClassroomModalVisible(false)}
      >
        <CreateClassroomForm onClassroomCreated={() => {
          setCreateClassroomModalVisible(false);
          loadClassrooms();
        }} />
      </Modal>
      <Modal
        title="Приєднатися до класу"
        centered
        footer={null}
        visible={joinClassroomModalVisible}
        onCancel={() => setJoinClassroomModalVisible(false)}
      >
        <JoinClassroomForm onJoined={() => {
          setJoinClassroomModalVisible(false);
          loadClassrooms();
        }}/>
      </Modal>
    </div>
  );
}

interface IClassroomsListProps {
  title: string,
  classrooms: IClassroom[],
}

export function ClassroomsList({ title, classrooms }:IClassroomsListProps) {
  return (
    <section>
      <h1 className="text-gray-300 text-lg">{title}</h1>
      <div className="max-w-3xl bg-gray-700 mx-auto rounded px-5 py-3 shadow-xl space-y-3">
        {classrooms.length === 0 ? <Empty description="Не знайдено такі класи"/> : (
          classrooms.map(classroom => <Classroom key={classroom._id} classroom={classroom} />)
        )}
      </div>
    </section>
  );
}

interface IClassroomProps {
  classroom: IClassroom,
}

export function Classroom({ classroom }:IClassroomProps) {
  const history = useHistory();
  const match = useRouteMatch();

  const goToClassroomPage = () => {
    history.push(`${match.path}/${classroom.urlCode}`);
  }

  return (
    <div
      className="rounded p-2 shadow-lg bg-gray-800 cursor-pointer hover:bg-gray-900"
      onClick={goToClassroomPage}
      role="button"
    >
      <h2 className="text-gray-50 mb-0 text-base">{classroom.name}</h2>
      <div className="flex flex-col sm:flex-row text-gray-400">
        <p className="mb-0">{classroom.description}</p>
        <p className="ml-auto mb-0">
          <span className="text-gray-200">Керівник:</span>
          {` ${classroom.classhead.firstName} ${classroom.classhead.lastName}`}</p>
      </div>
    </div>
  );
}
