import React, { useEffect } from 'react';
import { Form, Input, Button, message, Modal, Divider } from 'antd';
import { UserOutlined, LockOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import AuthAPI, { IUpdateUserDTO } from '../../api/auth.api';
import { IUserData } from '../../app/userSlice';

function showEditPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await AuthAPI.updateProfile(values);
        if (data) {
          localStorage.setItem('userData', JSON.stringify(data));
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          if (error.response && error.response.data.message === 'email reserved') {
            message.error('Ця електронна пошта вже зайнята іншим користувачем!');
          } else {
            message.error('Виникла помилка! Спробуйте пізніше.');
          }
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showPasswordPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Змінити пароль?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await AuthAPI.updatePassword(values);
        if (data) {
          afterCreation();
          message.success('Ви успішно змінили пароль!');
        } else if (error) {
          if (error.response && error.response.data.message === 'password') {
            message.error('Ви ввели неправильний поточний пароль!');
          } else {
            message.error('Виникла помилка! Спробуйте пізніше.');
          }
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

export default function EditProfileForm({ onEditProfile } : { onEditProfile?: () => any }) {
  const [form] = Form.useForm();
  const [passwordForm] = Form.useForm();

  const userData: IUserData = JSON.parse(localStorage.getItem('userData')!);

  const onFinish = async (values: IUpdateUserDTO) => {
    showEditPromiseConfirm(values, () => {
      onEditProfile && onEditProfile();
    });
  };

  const onFinishPassword = async (values: IUpdateUserDTO) => {
    showPasswordPromiseConfirm(values, () => {
      onEditProfile && onEditProfile();
    });
  };

  useEffect(() => {
    form.setFieldsValue({
      email: userData.email,
      firstName: userData.firstName,
      lastName: userData.lastName,
    })
  }, [userData]);

  return (
    <div>
      <Form
        form={form}
        name="edit-profile"
        className="login-form"
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            { required: true, message: 'Введіть свою електронну пошту!' },
            { pattern: /^\S+@\S+.\S+$/, message: 'Щось це не схоже на електронну пошту...' }
          ]}
        >
          <Input type="email" prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Електронна пошта" />
        </Form.Item>
        <Form.Item
          name="firstName"
          rules={[
            { required: true, message: 'Введіть своє ім\'я!' },
            { max: 50, message: 'Максимум 50 символів!' }
          ]}
        >
          <Input
            type="text"
            placeholder="Ім'я"
          />
        </Form.Item>
        <Form.Item
          name="lastName"
          rules={[
            { required: true, message: 'Введіть своє прізвище!' },
            { max: 50, message: 'Максимум 50 символів!' }
          ]}
        >
          <Input
            type="text"
            placeholder="Прізвище"
          />
        </Form.Item>
        <Form.Item className="mb-0">
          <Button type="primary" htmlType="submit" className="w-full">
            Зберегти зміни
          </Button>
        </Form.Item>
      </Form>
      <Divider></Divider>
      <h1 className="text-center">Змінити пароль</h1>
      <Form
        form={passwordForm}
        name="edit-password"
        className="login-form"
        onFinish={onFinishPassword}
      >
        <Form.Item
          name="oldPassword"
          rules={[
            { required: true, message: 'Введіть пароль!' },
            { min: 6, message: 'Мінімум 6 символів!' }
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Пароль"
          />
        </Form.Item>
        <Form.Item
          name="newPassword"
          rules={[
            { required: true, message: 'Введіть новий пароль!' },
            { min: 6, message: 'Мінімум 6 символів!' },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Новий пароль"
          />
        </Form.Item>
        <Form.Item className="mb-0">
          <Button type="primary" htmlType="submit" className="w-full">
            Змінити пароль
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}