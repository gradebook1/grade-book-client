import { WarningOutlined } from '@ant-design/icons';
import { Button, message, Modal } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import ClassroomsAPI from '../../api/classrooms.api';
import { deleteSelectedCourseShortInfo, selectClassroom } from '../../app/classroomSlice';
import DebounceSelect from '../../components/SearchAndSelectUsers/SearchAndSelectUsers';
import useUpdateClassroom from '../../hooks/updateClassroom.hook';
import { useAuthGuard } from '../../hooks/user.hooks';
import CreateClassroomForm from '../createClassroomForm/createClassroomForm';

const StyledClassroomForm = styled(CreateClassroomForm)`
  max-width: 500px;
  width: 100%;
  margin: 0 auto;
  padding-top: 5px;
  padding-bottom: 5px;

  & .ant-input {
    background-color: rgba(31, 41, 55, var(--tw-bg-opacity));
    color: whitesmoke;
    border: none;
  }

  & .ant-input-textarea-show-count::after {
    color: whitesmoke;
  }
`;

function showDeletePromiseConfirm(classroomId: string, afterCreation: () => any) {
  Modal.confirm({
    title: 'Підтвердження видалення',
    icon: <WarningOutlined />,
    content: 'Видалення класу видалить всі курси, завдання та оцінки всередині класу. Ви впевнені, що хочете видалити клас?',
    onOk() {
      async function request() {
        const { data, error } = await ClassroomsAPI.deleteClassroom(classroomId);
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили клас!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

interface UserTeachers {
  label: string;
  value: string;
}

export default function ClassroomSettings() {
  useAuthGuard();
  const updateClassroom = useUpdateClassroom();
  const classroomInfo = useSelector(selectClassroom);
  const dispatch = useDispatch();
  const history = useHistory();

  const [teachers, setTeachers] = React.useState<UserTeachers[]>([]);
  const [options, setOptions] = React.useState<UserTeachers[]>([]);

  async function fetchUserList(username: string): Promise<UserTeachers[]> {
    const { data } = await ClassroomsAPI.searchUsers(username, classroomInfo?._id || '');
    return data?.map(user => ({
      label: `${user.firstName} ${user.lastName}`,
      value: user._id,
    })) || [];
  }

  const handleAddTeacher = async () => {
    const { error } = await ClassroomsAPI.addTeacher(teachers.map(t => t.value), classroomInfo?._id || '');
    if (error) {
      message.error('Виникла помилка! Спробуйте пізніше.');
    } else {
      message.success('Ви успішно додали нових викладачів!');
      setTeachers([]);
      setOptions([]);
      updateClassroom();
    }
  }

  const handleDelete = () => {
    showDeletePromiseConfirm(classroomInfo?._id || '', () => {
      history.push('/app');
    });
  }

  useEffect(() => {
    dispatch(deleteSelectedCourseShortInfo());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="rounded-lg bg-gray-700 p-5 space-y-6">
      <h1 className="text-center text-gray-50 text-xl">Налаштування класу</h1>
      <StyledClassroomForm editMode onClassroomCreated={() => {
        updateClassroom();
      }} />
      <div className="text-center">
        <h2 className="text-gray-300 text-lg">
          Код класу: <span className="text-green-500 bg-gray-800 px-2 py-1 rounded">{classroomInfo?.code}</span>
        </h2>
        <p>За цим кодом користувачі зможуть приєднатися до вашого класу.</p>
      </div>
      <div className="text-center">
        <h2 className="text-gray-300 text-lg">
          Додати викладачів до класу
        </h2>
        <DebounceSelect
          mode="multiple"
          value={teachers}
          placeholder="Виберіть користувачів"
          fetchOptions={fetchUserList}
          options={options}
          setOptions={setOptions}
          onChange={newValue => {
            setTeachers(newValue);
          }}
          style={{ width: '100%', maxWidth: '500px' }}
        />
        <Button type="primary" className="rounded-tl-none rounded-bl-none" onClick={handleAddTeacher}>Додати</Button>
      </div>
      <div className="text-center">
        <Button danger type="primary" className="mt-3" onClick={handleDelete}>
          Видалити клас
        </Button>
      </div>
    </div>
  );
}
