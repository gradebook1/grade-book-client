import { CloseCircleFilled, WarningOutlined } from '@ant-design/icons';
import { Button, message, Modal } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import ClassroomsAPI from '../../api/classrooms.api';
import { deleteSelectedCourseShortInfo, selectClassroom, selectUserClassroomRole, UserClassroomRole } from '../../app/classroomSlice';
import { IUserData } from '../../app/userSlice';
import useUpdateClassroom from '../../hooks/updateClassroom.hook';
import { useAuthGuard } from '../../hooks/user.hooks';

function showDeleteTeacherPromiseConfirm(classroomId: string, teacherId: string, afterCreation: () => any) {
  Modal.confirm({
    title: 'Видалення викладача',
    icon: <WarningOutlined />,
    content: 'При видаленні викладача всі його курси перейдуть керівнику класу. Ви впевнені, що хочете видалити цього викладача?',
    onOk() {
      async function request() {
        const { data, error } = await ClassroomsAPI.deleteTeacher(teacherId, classroomId);
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили викладача!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

function showDeleteStudentPromiseConfirm(classroomId: string, studentId: string, afterCreation: () => any) {
  Modal.confirm({
    title: 'Видалення учня',
    icon: <WarningOutlined />,
    content: 'Ви впевнені, що хочете видалити цього учня?',
    onOk() {
      async function request() {
        const { data, error } = await ClassroomsAPI.deleteStudent(studentId, classroomId);
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили учня!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

export default function UsersList() {
  useAuthGuard();
  const updateClassroom = useUpdateClassroom();
  const classroomInfo = useSelector(selectClassroom);
  const history = useHistory();
  const dispatch = useDispatch();

  if (!classroomInfo) {
    history.push('/app');
  }

  const handleDeleteTeacher = (user: IUserData) => {
    showDeleteTeacherPromiseConfirm(classroomInfo?._id || '', user._id, () => {
      updateClassroom();
    });
  };

  const handleDeleteStudent = (user: IUserData) => {
    showDeleteStudentPromiseConfirm(classroomInfo?._id || '', user._id, () => {
      updateClassroom();
    });
  };

  useEffect(() => {
    dispatch(deleteSelectedCourseShortInfo());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="space-y-2">
      <UsersBlock onUserDelete={handleDeleteTeacher} title="Викладачі" users={[classroomInfo!.classhead, ...classroomInfo!.teachers]} />
      <UsersBlock onUserDelete={handleDeleteStudent} title="Учні/Студенти" users={classroomInfo!.students} />
    </div>
  );
}


function UsersBlock({ users, title, onUserDelete }:{users: IUserData[], title: string, onUserDelete?: (user: IUserData) => any}) {
  const classroomInfo = useSelector(selectClassroom);
  const userClassroomRole = useSelector(selectUserClassroomRole);

  return (
    <div className="max-w-2xl mx-auto rounded-lg bg-gray-700 p-5 shadow-lg">
      <div className="flex items-center">
        <h2 className="text-gray-50 text-2xl mb-0 space-y-2">
          {title}
        </h2>
        <span className="text-sm ml-auto">Кількість користувачів - {users.length}</span>
      </div>
      <div className="pt-4 space-y-3">
        {users.map((user, index) => (
          <div key={user._id}>
            <div className="px-4 py-1 flex items-center bg-gray-800 rounded">
              <span className="text-base">{user.firstName} {user.lastName}</span>
              {classroomInfo?.classhead !== user && userClassroomRole === UserClassroomRole.CLASSHEAD && <Button className="ml-auto flex items-center" danger ghost type="text" shape="circle" icon={<CloseCircleFilled />} onClick={() => onUserDelete && onUserDelete(user)} />}
            </div>
            {/* {index !== users.length - 1 && <Divider />} */}
          </div>
        ))}
        {users.length === 0 && <p className="text-center p-3">Користувачі відсутні.</p>}
      </div>
    </div>
  );
}