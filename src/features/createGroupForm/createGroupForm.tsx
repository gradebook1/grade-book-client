import React, { useEffect } from 'react';
import { Form, Input, Button, Modal, message, Select } from 'antd';
import { ExclamationCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectClassroom } from '../../app/classroomSlice';
import { useHistory } from 'react-router';
import CoursesAPI from '../../api/courses.api';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectCourse } from '../../app/courseSlice';
import GroupsAPI, { IDeleteGroupDTO } from '../../api/groups.api';
import { selectSelectedGroupForModal } from '../../app/tasksSlice';

const { Option } = Select;

function showEditPromiseConfirm(groupId: string, values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await GroupsAPI.updateGroup(groupId, values);
        if (data) {
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showDeletePromiseConfirm(groupId: string, deleteGroupDto: IDeleteGroupDTO, afterCreation: () => any) {
  Modal.confirm({
    title: 'Підтвердження видалення',
    icon: <WarningOutlined />,
    content: 'Підтвердіть видалення.',
    onOk() {
      async function request() {
        const { data, error } = await GroupsAPI.deleteGroup(groupId, deleteGroupDto);
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили групу!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

function showPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Створити нову групу?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть створення нової групи.',
    onOk() {
      async function request() {
        const { data, error } = await GroupsAPI.createGroup(values);
        if (data) {
          afterCreation();
          message.success('Ви успішно створили нову групу!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

interface ICreateGroupFormProps {
  onGroupCreated: () => any;
  editMode?: boolean;
}

export default function CreateGroupForm({ onGroupCreated, editMode = false }:ICreateGroupFormProps) {
  const [form] = Form.useForm();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const group = useSelector(selectSelectedGroupForModal);
  const history = useHistory();

  if (!classroomInfo) {
    history.push('/app');
  }

  const afterCreation = () => {
    if (!editMode) {
      form.resetFields();
    }
    onGroupCreated();
  };

  const onFinish = async (values: any) => {
    values.course = courseInfo?._id;
    console.log(values);
    if (editMode) {
      showEditPromiseConfirm(group?._id || '', values, afterCreation);
    } else {
      showPromiseConfirm(values, afterCreation);
    }
  };

  const handleDelete = () => {
    showDeletePromiseConfirm(group?._id || '', { course: courseInfo?._id || '' }, afterCreation);
  }

  useEffect(() => {
    if (!editMode) return;

    form.setFieldsValue({
      name: group?.name,
      gradeType: group?.gradeType,
    });
  }, [editMode, group]);

  return (
    <>
    <Form
      form={form}
      name="createGroup"
      onFinish={onFinish}
    >
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Введіть назву групи!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Назва групи"
        />
      </Form.Item>
      <Form.Item name="course" hidden>
        <Input type="text" value={courseInfo?._id}></Input>
      </Form.Item>
      <Form.Item name="gradeType" rules={[{ required: true, message: 'Виберіть спосіб вирахування оцінки!' }]}>
        <Select
          placeholder="Виберіть спосіб вирахування оцінки"
        >
          <Option value={GradeCalculationType.AVERAGE}>Середнє арифметичне всіх оцінок</Option>
          <Option value={GradeCalculationType.SUM}>Сума всіх оцінок</Option>
        </Select>
      </Form.Item>
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit">
          {editMode ? 'Зберегти зміни' : 'Створити групу'}
        </Button>
      </Form.Item>
    </Form>
    {editMode && (
      <Button danger type="primary" className="mt-3" onClick={handleDelete}>
        Видалити
      </Button>
    )}
    </>
  );
}