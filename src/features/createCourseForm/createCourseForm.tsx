import React, { useEffect, useState } from 'react';
import { Form, Input, Button, Modal, message, Select, Switch } from 'antd';
import { ExclamationCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectClassroom } from '../../app/classroomSlice';
import { useHistory } from 'react-router';
import SelectUsersTable from '../../components/SelectUsersTable/SelectUsersTable';
import CoursesAPI, { IDeleteCourseDTO } from '../../api/courses.api';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectCourse } from '../../app/courseSlice';
import { useParams } from 'react-router-dom';

const { Option } = Select;

function showEditPromiseConfirm(courseId: string, values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await CoursesAPI.updateCourse(courseId, values);
        if (data) {
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showDeletePromiseConfirm(courseId: string, deleteCourseDto: IDeleteCourseDTO, afterCreation: (courseDeleted?: boolean) => any) {
  Modal.confirm({
    title: 'Підтвердження видалення',
    icon: <WarningOutlined />,
    content: 'Видалення курсу видалить всі завдання та оцінки всередині курсу. Ви впевнені, що хочете видалити курс?',
    onOk() {
      async function request() {
        const { data, error } = await CoursesAPI.deleteCourse(courseId, deleteCourseDto);
        if (data) {
          afterCreation(true);
          message.success('Ви успішно видалили курс!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

function showPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Створити новий курс?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть створення нового курсу.',
    onOk() {
      async function request() {
        const { data, error } = await CoursesAPI.createCourse(values);
        if (data) {
          afterCreation();
          message.success('Ви успішно створили новий курс!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

interface ICreateCourseFormProps {
  onCourseCreated: () => any;
  editMode?: boolean;
}

export default function CreateCourseForm({ onCourseCreated, editMode = false }:ICreateCourseFormProps) {
  const [form] = Form.useForm();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const history = useHistory();
  const [isTableVisible, setTableVisible] = useState(false);
  const [editingStarted, setEditingStarted] = useState(false);
  const [r, rerender] = useState(false);
  const { classroomUrlCode } = useParams<{ classroomUrlCode: string }>();

  if (!classroomInfo) {
    history.push('/app');
  }

  const afterCreation = (courseDeleted?: boolean) => {
    if (!editMode) {
      form.resetFields();
    }
    if (courseDeleted) {
      history.push(`/app/${classroomUrlCode}`);
    }
    onCourseCreated();
  };

  const onFinish = async (values: any) => {
    values.classroom = classroomInfo?._id;
    if (values.isPrivate === undefined) {
      values.isPrivate = false;
    }
    console.log(values);
    if (editMode) {
      showEditPromiseConfirm(courseInfo?._id || '', values, afterCreation);
    } else {
      showPromiseConfirm(values, afterCreation);
    }
  };

  const handleDelete = () => {
    showDeletePromiseConfirm(courseInfo?._id || '', { classroom: classroomInfo?._id || '' }, afterCreation);
  }

  useEffect(() => {
    if (!editMode) return;

    form.setFieldsValue({
      name: courseInfo?.name,
      teacher: courseInfo?.teacher._id,
      gradeType: courseInfo?.gradeType,
      isPrivate: courseInfo?.isPrivate,
      privateStudents: courseInfo?.privateStudents?.map(user => user._id),
    });

    if (editMode && courseInfo?.isPrivate) {
      setTableVisible(true);
    } else {
      setTableVisible(false);
    }
  }, [editMode, courseInfo]);

  return (
    <>
    <Form
      form={form}
      name="createCourse"
      onFinish={onFinish}
    >
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Введіть назву курсу!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Назва курсу"
        />
      </Form.Item>
      <Form.Item name="classroom" hidden>
        <Input type="text" value={classroomInfo?._id}></Input>
      </Form.Item>
      <Form.Item name="teacher" rules={[{ required: true, message: 'Виберіть викладача!' }]}>
        <Select
          placeholder="Виберіть викладача цього курсу"
          showSearch
        >
          {[...classroomInfo!.teachers, classroomInfo!.classhead].map(teacher => <Option key={teacher._id} value={teacher._id}>{`${teacher.firstName} ${teacher.lastName}`}</Option>)}
        </Select>
      </Form.Item>
      <Form.Item name="gradeType" rules={[{ required: true, message: 'Виберіть спосіб вирахування оцінки!' }]}>
        <Select
          placeholder="Виберіть спосіб вирахування оцінки"
        >
          <Option value={GradeCalculationType.AVERAGE}>Середнє арифметичне всіх оцінок</Option>
          <Option value={GradeCalculationType.SUM}>Сума всіх оцінок</Option>
        </Select>
      </Form.Item>
      <Form.Item label="Приватний курс" name="isPrivate">
        <Switch checked={editingStarted ? isTableVisible : (editMode && courseInfo?.isPrivate)} onChange={(checked) => {
          setTableVisible(checked);
          if (!checked) {
            form.setFieldsValue({ privateStudents: [] });
          }
          setEditingStarted(true);
        }} />
      </Form.Item>
      {isTableVisible && <Form.Item name="privateStudents">
        <SelectUsersTable
          selectionType="checkbox"
          users={classroomInfo!.students}
          selectedUsers={form.getFieldValue('privateStudents')}
          onChange={(usersKeys) => {
            form.setFieldsValue({privateStudents: usersKeys});
            rerender(!r);
          }}
        />
      </Form.Item>}
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit">
          {editMode ? 'Зберегти зміни' : 'Створити курс'}
        </Button>
      </Form.Item>
    </Form>
    {editMode && (
      <Button danger type="primary" className="mt-3" onClick={handleDelete}>
        Видалити
      </Button>
    )}
    </>
  );
}
