import { message } from 'antd';
import React, {  } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import SubgroupsAPI from '../../api/subgroups.api';
import { IGrade } from '../../api/tasks.api';
import { selectClassroom } from '../../app/classroomSlice';
import { selectCourse } from '../../app/courseSlice';
import { selectGroups, selectSelectedSubgroupForGrades, selectTasks } from '../../app/tasksSlice';
import UsersGradesTable from '../../components/UsersGradesTable/UsersGradesTable';
import { getUserOnlyTasksAndOther, getUserSubgroupGrade } from '../../services/getCourseGrade';

export default function SubgroupGrades() {
  const dispatch = useDispatch();
  const history = useHistory();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const courseLevelTasks = useSelector(selectTasks);
  const groups = useSelector(selectGroups);
  const selectedSubgroup = useSelector(selectSelectedSubgroupForGrades);

  if (!courseInfo) {
    history.push(`/app/${classroomInfo?.urlCode}`);
    return null;
  }

  if (!courseLevelTasks || !groups || !selectedSubgroup) {
    history.push(`/app/${classroomInfo?.urlCode}/c/${courseInfo?.urlCode}`);
    return null;
  }

  const users = courseInfo?.isPrivate ? courseInfo.privateStudents : classroomInfo?.students;

  const saveGrade = async (userId: string, grade?: number) => {
    const { error } = await SubgroupsAPI.updateSubgroupGrade({
      course: courseInfo._id || '',
      subgroup: selectedSubgroup._id || '',
      student: userId,
      grade: grade,
    });
    
    if (error) {
      message.error('Виникла помилка при оновленні оцінки! Спробуйте пізніше.');
    }
  }

  const autoGrades: IGrade[] | undefined = users?.map(user => {
    const tasksAndGroups = getUserOnlyTasksAndOther(user, courseLevelTasks, groups);
    const groupContainsSubgroup = tasksAndGroups.groups.find(g => g.subgroups.find(sg => sg._id === selectedSubgroup._id));
    const subgroupGrade = getUserSubgroupGrade(
      selectedSubgroup.gradeType,
      groupContainsSubgroup?.subgroups.find(sg => sg._id === selectedSubgroup._id)?.tasks,
      groupContainsSubgroup?.subgroups.find(sg => sg._id === selectedSubgroup._id)?.modules,
    );
    return { student: user._id, grade: subgroupGrade };
  });

  // console.log(autoGrades);

  return (
    <div>
      {/* TODO: Add button to go back to course main page (tasks) */}
      <h2 className="text-center text-gray-600 mb-0">{`Курс '${courseInfo.name}'`}</h2>
      <h3 className="text-center text-gray-600">{`Група '${groups.find(g => g.subgroups.find(sg => sg._id === selectedSubgroup._id))?.name}'`}</h3>
      <h4 className="text-center text-gray-800 text-lg">{`Підгрупа '${selectedSubgroup.name}'`}</h4>
      <UsersGradesTable
        users={users}
        grades={selectedSubgroup.grades}
        autoGrades={autoGrades}
        onGradeChange={saveGrade}/>
    </div>
  );
}
