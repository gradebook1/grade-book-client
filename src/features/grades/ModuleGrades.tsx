import { message } from 'antd';
import React, {  } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import ModulesAPI from '../../api/modules.api';
import { IGrade } from '../../api/tasks.api';
import { selectClassroom } from '../../app/classroomSlice';
import { selectCourse } from '../../app/courseSlice';
import { selectGroups, selectSelectedModuleForGrades, selectTasks } from '../../app/tasksSlice';
import UsersGradesTable from '../../components/UsersGradesTable/UsersGradesTable';
import { getUserModuleGrade, getUserOnlyTasksAndOther } from '../../services/getCourseGrade';

export default function ModuleGrades() {
  const history = useHistory();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const courseLevelTasks = useSelector(selectTasks);
  const groups = useSelector(selectGroups);
  const selectedModule = useSelector(selectSelectedModuleForGrades);

  if (!courseInfo) {
    history.push(`/app/${classroomInfo?.urlCode}`);
    return null;
  }

  if (!courseLevelTasks || !groups || !selectedModule) {
    history.push(`/app/${classroomInfo?.urlCode}/c/${courseInfo?.urlCode}`);
    return null;
  }

  const users = courseInfo?.isPrivate ? courseInfo.privateStudents : classroomInfo?.students;

  const saveGrade = async (userId: string, grade?: number) => {
    const { error } = await ModulesAPI.updateModuleGrade({
      course: courseInfo._id || '',
      module: selectedModule._id || '',
      student: userId,
      grade: grade,
    });
    
    if (error) {
      message.error('Виникла помилка при оновленні оцінки! Спробуйте пізніше.');
    }
  }

  const autoGrades: IGrade[] | undefined = users?.map(user => {
    const tasksAndGroups = getUserOnlyTasksAndOther(user, courseLevelTasks, groups);
    const groupContainsModule = tasksAndGroups.groups.find(g => g.subgroups.find(sg => sg.modules.find(m => m._id === selectedModule._id)));
    const subgroupContainsModule = groupContainsModule?.subgroups.find(sg => sg.modules.find(m => m._id === selectedModule._id));
    const moduleGrade = getUserModuleGrade(
      selectedModule.gradeType,
      subgroupContainsModule?.modules.find(m => m._id === selectedModule._id)?.tasks,
    );
    return { student: user._id, grade: moduleGrade };
  });

  // console.log(autoGrades);

  const groupContainsModule = groups.find(g => g.subgroups.find(sg => sg.modules.find(m => m._id === selectedModule._id)));
  const subgroupContainsModule = groupContainsModule?.subgroups.find(sg => sg.modules.find(m => m._id === selectedModule._id));

  return (
    <div>
      {/* TODO: Add button to go back to course main page (tasks) */}
      <h2 className="text-center text-gray-800 mb-0">{`Курс '${courseInfo.name}'`}</h2>
      <h3 className="text-center text-gray-800">{`Група '${groupContainsModule?.name}'`}</h3>
      <h4 className="text-center text-gray-800">{`Підгрупа '${subgroupContainsModule?.name}'`}</h4>
      <h5 className="text-center text-gray-600 text-lg">{`Модуль '${selectedModule.name}'`}</h5>
      <UsersGradesTable
        users={users}
        grades={selectedModule.grades}
        autoGrades={autoGrades}
        onGradeChange={saveGrade}/>
    </div>
  );
}
