import { message } from 'antd';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import CoursesAPI from '../../api/courses.api';
import { IGrade } from '../../api/tasks.api';
import { selectClassroom } from '../../app/classroomSlice';
import { selectCourse } from '../../app/courseSlice';
import { selectGroups, selectTasks } from '../../app/tasksSlice';
import UsersGradesTable from '../../components/UsersGradesTable/UsersGradesTable';
import { getUserCourseGrade, getUserOnlyTasksAndOther } from '../../services/getCourseGrade';

export default function CourseGrades() {
  const dispatch = useDispatch();
  const history = useHistory();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const courseLevelTasks = useSelector(selectTasks);
  const groups = useSelector(selectGroups);

  const [isLoading, setLoading] = useState(true);
  const [isError, setError] = useState(false);
  const { classroomUrlCode, courseUrlCode } = useParams<{ classroomUrlCode: string, courseUrlCode: string }>();

  if (!courseInfo) {
    // NOTE: Removed since it is now not separate page and when we change course we remove it from store
    // and then set again. CourseGrades component now always on the page (because it is in modal now)
    // so during that time when we have no course next line causes broser to go to classroom page when
    // we actually just want to select another course.
    // history.push(`/app/${classroomInfo?.urlCode}`);
    return null;
  }

  if (!courseLevelTasks || !groups) {
    history.push(`/app/${classroomInfo?.urlCode}/c/${courseInfo?.urlCode}`);
    return null;
  }

  // TODO: Maybe just push history to course page ? :\ Would be much easier than try to reload here tasks i guess
  // const loadAllTasks = async () => {
  //   setLoading(true);
  //   setError(false);
  //   const { data, error } = await TasksAPI.getAllTasks(courseUrlCode, classroomUrlCode);
  //   if (data) {
  //     dispatch(setTasks({ tasks: data.tasks }));
  //     setLoading(false);
  //   } else if (error) {
  //     if (error.response && error.response.data.message === 'no classroom') {
  //       history.push('/app');
  //     } else if (error.response && error.response.data.message === 'no course') {
  //       history.push(`/app/${classroomUrlCode}`);
  //     }
  //     // TODO: Handle here 403 forbidden error to show that user doesnt have access to that private course
  //     setError(true);
  //     setLoading(false);
  //   }
  // }

  // useEffect(() => {
  //   if (!courseLevelTasks) {
  //     loadAllTasks();
  //   }
  // }, []);

  // let content;
  // if (isLoading) {
  //   content = <Loader />;
  // } else if (isError) {
  //   content = <h2 className="text-gray-300 text-center">Помилка при завантажені даних. Спробуйте пізніше.</h2>
  // } else {
  //   content = (
  //     <div id="tasks">
  //       {courseLevelTasks && <TasksList tasks={courseLevelTasks} />}
  //     </div>
  //   );
  // }

  const users = courseInfo?.isPrivate ? courseInfo.privateStudents : classroomInfo?.students;

  const saveGrade = async (userId: string, grade?: number) => {
    const { error } = await CoursesAPI.updateCourseGrade({
      course: courseInfo?._id || '',
      student: userId,
      grade: grade,
    });
    
    if (error) {
      message.error('Виникла помилка при оновленні оцінки! Спробуйте пізніше.');
    }
  }

  const autoGrades: IGrade[] | undefined = users?.map(user => {
    const tasksAndGroups = getUserOnlyTasksAndOther(user, courseLevelTasks, groups);
    const courseGrade = getUserCourseGrade(courseInfo!.gradeType, tasksAndGroups.tasks, tasksAndGroups.groups);
    return { student: user._id, grade: courseGrade };
  });

  // console.log(autoGrades);

  return (
    <div>
      {/* TODO: Add button to go back to course main page (tasks) */}
      <h2 className="text-center text-gray-600 mb-0">Курс</h2>
      <h3 className="text-center text-gray-800 text-lg">{courseInfo?.name}</h3>
      <UsersGradesTable
        users={users}
        grades={courseInfo?.grades}
        autoGrades={autoGrades}
        onGradeChange={saveGrade}/>
    </div>
  );
}
