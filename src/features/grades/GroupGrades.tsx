import { message } from 'antd';
import React, {  } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import GroupsAPI from '../../api/groups.api';
import { IGrade } from '../../api/tasks.api';
import { selectClassroom } from '../../app/classroomSlice';
import { selectCourse } from '../../app/courseSlice';
import { selectGroups, selectSelectedGroupForGrades, selectTasks } from '../../app/tasksSlice';
import UsersGradesTable from '../../components/UsersGradesTable/UsersGradesTable';
import { getUserGroupGrade, getUserOnlyTasksAndOther } from '../../services/getCourseGrade';

export default function GroupGrades() {
  const dispatch = useDispatch();
  const history = useHistory();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const courseLevelTasks = useSelector(selectTasks);
  const groups = useSelector(selectGroups);
  const selectedGroup = useSelector(selectSelectedGroupForGrades);

  if (!courseInfo) {
    history.push(`/app/${classroomInfo?.urlCode}`);
    return null;
  }

  if (!courseLevelTasks || !groups || !selectedGroup) {
    history.push(`/app/${classroomInfo?.urlCode}/c/${courseInfo?.urlCode}`);
    return null;
  }

  const users = courseInfo?.isPrivate ? courseInfo.privateStudents : classroomInfo?.students;

  const saveGrade = async (userId: string, grade?: number) => {
    const { error } = await GroupsAPI.updateGroupGrade({
      course: courseInfo._id || '',
      group: selectedGroup._id || '',
      student: userId,
      grade: grade,
    });
    
    if (error) {
      message.error('Виникла помилка при оновленні оцінки! Спробуйте пізніше.');
    }
  }

  const autoGrades: IGrade[] | undefined = users?.map(user => {
    const tasksAndGroups = getUserOnlyTasksAndOther(user, courseLevelTasks, groups);
    const groupGrade = getUserGroupGrade(
      selectedGroup.gradeType,
      tasksAndGroups.groups.find(g => g._id === selectedGroup._id)?.tasks,
      tasksAndGroups.groups.find(g => g._id === selectedGroup._id)?.subgroups,
    );
    return { student: user._id, grade: groupGrade };
  });

  // console.log(autoGrades);

  return (
    <div>
      {/* TODO: Add button to go back to course main page (tasks) */}
      <h2 className="text-center text-gray-600 mb-0">{`Курс '${courseInfo.name}'`}</h2>
      <h3 className="text-center text-gray-800 text-lg">{`Група '${selectedGroup.name}'`}</h3>
      <UsersGradesTable
        users={users}
        grades={selectedGroup.grades}
        autoGrades={autoGrades}
        onGradeChange={saveGrade}/>
    </div>
  );
}
