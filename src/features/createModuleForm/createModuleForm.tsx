import React, { useEffect } from 'react';
import { Form, Input, Button, Modal, message, Select } from 'antd';
import { ExclamationCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectClassroom } from '../../app/classroomSlice';
import { useHistory } from 'react-router';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectCourse } from '../../app/courseSlice';
import { selectSelectedModuleForModal, selectSelectedSubgroupForModal } from '../../app/tasksSlice';
import ModulesAPI, { IDeleteModuleDTO } from '../../api/modules.api';
import { ISubgroupPopulated } from '../../api/subgroups.api';

const { Option } = Select;

function showEditPromiseConfirm(moduleId: string, values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await ModulesAPI.updateModule(moduleId, values);
        if (data) {
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showDeletePromiseConfirm(moduleId: string, deleteModuleDto: IDeleteModuleDTO, afterCreation: () => any) {
  Modal.confirm({
    title: 'Підтвердження видалення',
    icon: <WarningOutlined />,
    content: 'Підтвердіть видалення.',
    onOk() {
      async function request() {
        const { data, error } = await ModulesAPI.deleteModule(moduleId, deleteModuleDto);
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили модуль!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

function showPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Створити новий модуль?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть створення нового модуля.',
    onOk() {
      async function request() {
        const { data, error } = await ModulesAPI.createModule(values);
        if (data) {
          afterCreation();
          message.success('Ви успішно створили новий модуль!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

interface ICreateModuleFormProps {
  onModuleCreated: () => any;
  editMode?: boolean;
  subgroupProp?: ISubgroupPopulated;
}

export default function CreateModuleForm({ onModuleCreated, editMode = false, subgroupProp }:ICreateModuleFormProps) {
  const [form] = Form.useForm();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const subgroup = useSelector(selectSelectedSubgroupForModal);
  const module = useSelector(selectSelectedModuleForModal);
  const history = useHistory();

  if (!classroomInfo) {
    history.push('/app');
  }

  const afterCreation = () => {
    if (!editMode) {
      form.resetFields();
    }
    onModuleCreated();
  };

  const onFinish = async (values: any) => {
    values.course = courseInfo?._id;
    values.subgroup = subgroup?._id;
    console.log(values);
    if (editMode) {
      showEditPromiseConfirm(module?._id || '', values, afterCreation);
    } else {
      showPromiseConfirm(values, afterCreation);
    }
  };

  const handleDelete = () => {
    showDeletePromiseConfirm(module?._id || '', { course: courseInfo?._id || '', subgroup: subgroup?._id || subgroupProp?._id || '' }, afterCreation);
  }

  useEffect(() => {
    if (!editMode) return;

    form.setFieldsValue({
      name: module?.name,
      gradeType: module?.gradeType,
    });
  }, [editMode, module]);

  return (
    <>
    <Form
      form={form}
      name="createModule"
      onFinish={onFinish}
    >
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Введіть назву модуля!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Назва модуля"
        />
      </Form.Item>
      <Form.Item name="course" hidden>
        <Input type="text" value={courseInfo?._id}></Input>
      </Form.Item>
      <Form.Item name="gradeType" rules={[{ required: true, message: 'Виберіть спосіб вирахування оцінки!' }]}>
        <Select
          placeholder="Виберіть спосіб вирахування оцінки"
        >
          <Option value={GradeCalculationType.AVERAGE}>Середнє арифметичне всіх оцінок</Option>
          <Option value={GradeCalculationType.SUM}>Сума всіх оцінок</Option>
        </Select>
      </Form.Item>
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit">
          {editMode ? 'Зберегти зміни' : 'Створити модуль'}
        </Button>
      </Form.Item>
    </Form>
    {editMode && (
      <Button danger type="primary" className="mt-3" onClick={handleDelete}>
        Видалити
      </Button>
    )}
    </>
  );
}