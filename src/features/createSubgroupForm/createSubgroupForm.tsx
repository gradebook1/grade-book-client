import React, { useEffect } from 'react';
import { Form, Input, Button, Modal, message, Select } from 'antd';
import { ExclamationCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { selectClassroom } from '../../app/classroomSlice';
import { useHistory } from 'react-router';
import { GradeCalculationType } from '../../api/tasks.api';
import { selectCourse } from '../../app/courseSlice';
import { selectSelectedGroupForModal, selectSelectedSubgroupForModal } from '../../app/tasksSlice';
import SubgroupsAPI, { IDeleteSubgroupDTO } from '../../api/subgroups.api';
import { IGroupPopulated } from '../../api/groups.api';

const { Option } = Select;

function showEditPromiseConfirm(subgroupId: string, values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Зберегти зміни?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть внесення змін.',
    onOk() {
      async function request() {
        const { data, error } = await SubgroupsAPI.updateSubgroup(subgroupId, values);
        if (data) {
          afterCreation();
          message.success('Ви успішно зберегли зміни!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

function showDeletePromiseConfirm(subgroupId: string, deleteSubgroupDto: IDeleteSubgroupDTO, afterCreation: () => any) {
  Modal.confirm({
    title: 'Підтвердження видалення',
    icon: <WarningOutlined />,
    content: 'Підтвердіть видалення.',
    onOk() {
      async function request() {
        const { data, error } = await SubgroupsAPI.deleteSubgroup(subgroupId, deleteSubgroupDto);
        if (data) {
          afterCreation();
          message.success('Ви успішно видалили підгрупу!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Закрити',
    okText: 'Видалити',
    okType: 'danger',
  });
}

function showPromiseConfirm(values: any, afterCreation: () => any) {
  Modal.confirm({
    title: 'Створити нову підгрупу?',
    icon: <ExclamationCircleOutlined />,
    content: 'Підтвердіть створення нової підгрупи.',
    onOk() {
      async function request() {
        const { data, error } = await SubgroupsAPI.createSubgroup(values);
        if (data) {
          afterCreation();
          message.success('Ви успішно створили нову підгрупу!');
        } else if (error) {
          message.error('Виникла помилка! Спробуйте пізніше.');
        }
      }

      return request();
    },
    onCancel() {},
    centered: true,
    cancelText: 'Ні',
    okText: 'Так',
  });
}

interface ICreateSubgroupFormProps {
  onSubgroupCreated: () => any;
  editMode?: boolean;
  groupProp?: IGroupPopulated;
}

export default function CreateSubgroupForm({ onSubgroupCreated, editMode = false, groupProp }:ICreateSubgroupFormProps) {
  const [form] = Form.useForm();
  const classroomInfo = useSelector(selectClassroom);
  const courseInfo = useSelector(selectCourse);
  const group = useSelector(selectSelectedGroupForModal);
  const subgroup = useSelector(selectSelectedSubgroupForModal);
  const history = useHistory();

  if (!classroomInfo) {
    history.push('/app');
  }

  const afterCreation = () => {
    if (!editMode) {
      form.resetFields();
    }
    onSubgroupCreated();
  };

  const onFinish = async (values: any) => {
    values.course = courseInfo?._id;
    values.group = group?._id;
    console.log(values);
    if (editMode) {
      showEditPromiseConfirm(subgroup?._id || '', values, afterCreation);
    } else {
      showPromiseConfirm(values, afterCreation);
    }
  };

  const handleDelete = () => {
    showDeletePromiseConfirm(subgroup?._id || '', { course: courseInfo?._id || '', group: group?._id || groupProp?._id || '' }, afterCreation);
  }

  useEffect(() => {
    if (!editMode) return;

    form.setFieldsValue({
      name: subgroup?.name,
      gradeType: subgroup?.gradeType,
    });
  }, [editMode, subgroup]);

  return (
    <>
    <Form
      form={form}
      name="createSubgroup"
      onFinish={onFinish}
    >
      <Form.Item
        name="name"
        rules={[
          { required: true, message: 'Введіть назву підгрупи!' },
          { max: 50, message: 'Максимум 50 символів!' }
        ]}
      >
        <Input
          type="text"
          placeholder="Назва підгрупи"
        />
      </Form.Item>
      <Form.Item name="course" hidden>
        <Input type="text" value={courseInfo?._id}></Input>
      </Form.Item>
      <Form.Item name="gradeType" rules={[{ required: true, message: 'Виберіть спосіб вирахування оцінки!' }]}>
        <Select
          placeholder="Виберіть спосіб вирахування оцінки"
        >
          <Option value={GradeCalculationType.AVERAGE}>Середнє арифметичне всіх оцінок</Option>
          <Option value={GradeCalculationType.SUM}>Сума всіх оцінок</Option>
        </Select>
      </Form.Item>
      <Form.Item className="mb-0">
        <Button type="primary" htmlType="submit">
          {editMode ? 'Зберегти зміни' : 'Створити підгрупу'}
        </Button>
      </Form.Item>
    </Form>
    {editMode && (
      <Button danger type="primary" className="mt-3" onClick={handleDelete}>
        Видалити
      </Button>
    )}
    </>
  );
}