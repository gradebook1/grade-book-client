import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { GradeCalculationType, IGrade } from '../api/tasks.api';
import { RootState } from './store';
import { IUserData } from './userSlice';

export enum UserCourseRole {
  COURSE_TEACHER = 'courseteacher',
  STUDENT = 'student',
  TEACHER = 'teacher',
}

export interface ICoursePopulated {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  urlCode: string;
  classroom: string;
  teacher: IUserData;
  isPrivate: boolean;
  privateStudents?: IUserData[];
  gradeType: GradeCalculationType;
  grades: IGrade[];
  // groups: IGroup[]; TODO
}

interface CourseState {
  course: ICoursePopulated | null;
  userCourseRole: UserCourseRole | null;
}

const initialState: CourseState = {
  course: null,
  userCourseRole: null,
};

export const courseSlice = createSlice({
  name: 'course',
  initialState,
  reducers: {
    setCourseInfo: (state, action: PayloadAction<{course: ICoursePopulated, role: UserCourseRole}>) => {
      state.course = action.payload.course;
      state.userCourseRole = action.payload.role;
    },
    deleteCourseInfo: (state) => {
      state.course = null;
      state.userCourseRole = null;
    },
  },
});

export const {
  setCourseInfo,
  deleteCourseInfo,
} = courseSlice.actions;

export const selectCourse = (state: RootState) => state.course.course;
export const selectUserCourseRole = (state: RootState) => state.course.userCourseRole;

export default courseSlice.reducer;
