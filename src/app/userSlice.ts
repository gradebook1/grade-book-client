// import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// import { RootState } from './store';

export interface IUserData {
  createdDate: string,
  email: string,
  firstName: string,
  lastName: string,
  userId: string, // TODO: remove usage
  _id: string,
}

// interface UserState {
//   userData: IUserData | null,
//   isAuthorized: boolean,
// }

// const initialState: UserState = {
//   isAuthorized: false,
//   userData: null,
// };

// export const userSlice = createSlice({
//   name: 'user',
//   initialState,
//   reducers: {
//     setUserData: (state, action: PayloadAction<IUserData>) => {
//       state.userData = action.payload;
//       state.isAuthorized = true;
//     },
//     deleteUserData: (state) => {
//       state.userData = null;
//       state.isAuthorized = false;
//     },
//   },
// });

// export const { setUserData } = userSlice.actions;

// export const selectUserData = (state: RootState) => state.user.userData;
// export const selectUserStatus = (state: RootState) => state.user.isAuthorized;

// export default userSlice.reducer;
