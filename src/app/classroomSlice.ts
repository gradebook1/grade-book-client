import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ICourseShortInfo } from '../api/courses.api';
import { RootState } from './store';
import { IUserData } from './userSlice';

export enum UserClassroomRole {
  CLASSHEAD = 'classhead',
  STUDENT = 'student',
  TEACHER = 'teacher',
}

export interface IClassroomPopulated {
  _id: string;
  name: string;
  description: string;
  createdDate: string;
  classhead: IUserData;
  teachers: IUserData[];
  students: IUserData[];
  urlCode: string;
  code?: string;
  courses: ICourseShortInfo[];
}

interface ClassroomState {
  classroom: IClassroomPopulated | null;
  userClassroomRole: UserClassroomRole | null;
  selectedCourseShortInfo: ICourseShortInfo | null;
}

const initialState: ClassroomState = {
  classroom: null,
  userClassroomRole: null,
  selectedCourseShortInfo: null,
};

export const classroomSlice = createSlice({
  name: 'classroom',
  initialState,
  reducers: {
    setClassroomInfo: (state, action: PayloadAction<{classroom: IClassroomPopulated, role: UserClassroomRole}>) => {
      state.classroom = action.payload.classroom;
      state.userClassroomRole = action.payload.role;
    },
    deleteClassroomInfo: (state) => {
      state.classroom = null;
      state.userClassroomRole = null;
    },
    setSelectedCourseShortInfo: (state, action: PayloadAction<ICourseShortInfo>) => {
      state.selectedCourseShortInfo = action.payload;
    },
    deleteSelectedCourseShortInfo: (state) => {
      state.selectedCourseShortInfo = null;
    },
  },
});

export const {
  setClassroomInfo,
  deleteClassroomInfo,
  setSelectedCourseShortInfo,
  deleteSelectedCourseShortInfo
} = classroomSlice.actions;

export const selectClassroom = (state: RootState) => state.classroom.classroom;
export const selectUserClassroomRole = (state: RootState) => state.classroom.userClassroomRole;
export const selectSelectedCourseShortInfo = (state: RootState) => state.classroom.selectedCourseShortInfo;

export default classroomSlice.reducer;
