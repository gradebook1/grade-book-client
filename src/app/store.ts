import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import classroomReducer from './classroomSlice';
import courseReducer from './courseSlice';
import tasksReducer from './tasksSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    classroom: classroomReducer,
    course: courseReducer,
    tasks: tasksReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
