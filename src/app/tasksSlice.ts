import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IGroupPopulated } from '../api/groups.api';
import { IModulePopulated } from '../api/modules.api';
import { ISubgroupPopulated } from '../api/subgroups.api';
import { ITask } from '../api/tasks.api';
import { RootState } from './store';

interface TasksState {
  tasks: ITask[] | null;
  selectedTaskForModal: ITask | null;
  selectedTaskForGrades: ITask | null;
  groups: IGroupPopulated[] | null;
  selectedGroupForModal: IGroupPopulated | null;
  selectedGroupForGrades: IGroupPopulated | null;
  selectedSubgroupForModal: ISubgroupPopulated | null;
  selectedSubgroupForGrades: ISubgroupPopulated | null;
  selectedModuleForModal: IModulePopulated | null;
  selectedModuleForGrades: IModulePopulated | null;
}

const initialState: TasksState = {
  tasks: null,
  selectedTaskForModal: null,
  selectedTaskForGrades: null,
  groups: null,
  selectedGroupForModal: null,
  selectedGroupForGrades: null,
  selectedSubgroupForModal: null,
  selectedSubgroupForGrades: null,
  selectedModuleForModal: null,
  selectedModuleForGrades: null,
};

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    setTasks: (state, action: PayloadAction<{tasks: ITask[], groups: IGroupPopulated[]}>) => {
      state.tasks = action.payload.tasks;
      state.groups = action.payload.groups;
    },
    setSelectedTaskForModal: (state, action: PayloadAction<ITask>) => {
      state.selectedTaskForModal = action.payload;
    },
    setSelectedTaskForGrades: (state, action: PayloadAction<ITask>) => {
      state.selectedTaskForGrades = action.payload;
    },
    deleteSelectedTaskForGrades: (state) => {
      state.selectedTaskForGrades = null;
    },
    setSelectedGroupForModal: (state, action: PayloadAction<IGroupPopulated>) => {
      state.selectedGroupForModal = action.payload;
    },
    setSelectedGroupForGrades: (state, action: PayloadAction<IGroupPopulated>) => {
      state.selectedGroupForGrades = action.payload;
    },
    setSelectedSubgroupForModal: (state, action: PayloadAction<ISubgroupPopulated>) => {
      state.selectedSubgroupForModal = action.payload;
    },
    setSelectedSubgroupForGrades: (state, action: PayloadAction<ISubgroupPopulated>) => {
      state.selectedSubgroupForGrades = action.payload;
    },
    setSelectedModuleForModal: (state, action: PayloadAction<IModulePopulated>) => {
      state.selectedModuleForModal = action.payload;
    },
    setSelectedModuleForGrades: (state, action: PayloadAction<IModulePopulated>) => {
      state.selectedModuleForGrades = action.payload;
    },
  },
});

export const {
  setTasks,
  setSelectedTaskForModal,
  setSelectedTaskForGrades,
  deleteSelectedTaskForGrades,
  setSelectedGroupForModal,
  setSelectedGroupForGrades,
  setSelectedSubgroupForModal,
  setSelectedSubgroupForGrades,
  setSelectedModuleForModal,
  setSelectedModuleForGrades,
} = tasksSlice.actions;

export const selectTasks = (state: RootState) => state.tasks.tasks;
export const selectGroups = (state: RootState) => state.tasks.groups;
export const selectSelectedTaskForModal = (state: RootState) => state.tasks.selectedTaskForModal;
export const selectSelectedTaskForGrades = (state: RootState) => state.tasks.selectedTaskForGrades;
export const selectSelectedGroupForModal = (state: RootState) => state.tasks.selectedGroupForModal;
export const selectSelectedGroupForGrades = (state: RootState) => state.tasks.selectedGroupForGrades;
export const selectSelectedSubgroupForModal = (state: RootState) => state.tasks.selectedSubgroupForModal;
export const selectSelectedSubgroupForGrades = (state: RootState) => state.tasks.selectedSubgroupForGrades;
export const selectSelectedModuleForModal = (state: RootState) => state.tasks.selectedModuleForModal;
export const selectSelectedModuleForGrades = (state: RootState) => state.tasks.selectedModuleForGrades;

export default tasksSlice.reducer;
