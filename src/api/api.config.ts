import axios, { AxiosError } from "axios";

axios.interceptors.response.use(res => res, (error: AxiosError) => {
  if (error.response && error.response.status === 401) {
    localStorage.removeItem('userData');
    // TODO: Clear token cookie ?
    // redirect to home page
    window.location.assign(window.location.origin); // TODO: Change since it doesnot work well with GitLab Pages
  }
  return Promise.reject(error);
});

// DEV: for development
// export const API_URL = 'http://localhost:3001';
export const API_URL = 'https://grade-book-api.herokuapp.com';


/**
 * Обёртка для отправки запросов.
 * @param action Function that performs request and must return response data
 * @returns Object with data and error information
 */
export async function doRequest<T>(action: () => Promise<any>) {
  try {
    const data = await action();
    return { data: data as T, error: null };
  } catch (error) {
    if ((error as AxiosError).response?.status === 401) {
      return { data: null, error: null };
    }
    return { data: null, error: error as AxiosError };
  }
}