import axios from "axios";
import { IClassroomPopulated, UserClassroomRole } from "../app/classroomSlice";
import { IUserData } from "../app/userSlice";
import { API_URL, doRequest } from "./api.config";

export interface IClassroom {
  _id: string;
  name: string;
  description: string;
  createdDate: string;
  classhead: IUserData;
  teachers: string[];
  students: string[];
  urlCode: string;
  code?: string;
  courses: string[];
}

export interface IClassroomsList {
  createdBy: IClassroom[];
  whereStudent: IClassroom[];
  whereTeacher: IClassroom[];
}

export interface ICreateClassroomDTO {
  name: string;
  description: string;
}

export interface IJoinClassroomDTO {
  code: string;
}

export interface IJoinClassroomResponseDto {
  name: string;
}

export interface IGetClassroomPageInfoResponseDto {
  classroom: IClassroomPopulated;
  role: UserClassroomRole;
}

export default class ClassroomsAPI {
  static getClassrooms() {
    // try {
    //   const response = await axios.post(`${API_URL}/classrooms`, {withCredentials: true});
    //   return { data: response.data as IClassroomsList, error: null };
    // } catch (error) {
    //   if ((error as AxiosError).response?.status === 401) {
    //     return { data: null, error: null };
    //   }
    //   return { data: null, error: error as AxiosError };
    // }
    return doRequest<IClassroomsList>(async () => {
      const response = await axios.get(`${API_URL}/classrooms`, {withCredentials: true});
      return response.data;
    });
  }

  static createClassroom(createClassroomDTO: ICreateClassroomDTO) {
    return doRequest<IClassroom>(async () => {
      const response = await axios.post(`${API_URL}/classrooms`, createClassroomDTO, {withCredentials: true});
      return response.data;
    });
  }

  static joinClassroom(joinClassroomDto: IJoinClassroomDTO) {
    return doRequest<IJoinClassroomResponseDto>(async () => {
      const response = await axios.post(`${API_URL}/classrooms/join`, joinClassroomDto, {withCredentials: true});
      return response.data;
    });
  }

  static getInfoForClassroomPage(classroomUrlCode: string) {
    return doRequest<IGetClassroomPageInfoResponseDto>(async () => {
      const response = await axios.get(`${API_URL}/classrooms/${classroomUrlCode}`, {withCredentials: true});
      return response.data;
    });
  }

  static updateClassroom(classroomId: string, createClassroomDTO: ICreateClassroomDTO) {
    return doRequest<IClassroom>(async () => {
      const response = await axios.put(`${API_URL}/classrooms/${classroomId}`, createClassroomDTO, {withCredentials: true});
      return response.data;
    });
  }

  static searchUsers(username: string, classroomId: string) {
    return doRequest<IUserData[]>(async () => {
      const response = await axios.post(`${API_URL}/classrooms/${classroomId}/searchusers`, { username }, {withCredentials: true});
      return response.data;
    });
  }

  static addTeacher(userIds: string[], classroomId: string) {
    return doRequest<any>(async () => {
      const response = await axios.post(`${API_URL}/classrooms/${classroomId}/teachers`, { userIds }, {withCredentials: true});
      return response.data;
    });
  }

  static deleteTeacher(teacherId: string, classroomId: string) {
    return doRequest<any>(async () => {
      const response = await axios.post(`${API_URL}/classrooms/${classroomId}/teachers/delete`, { teacherId }, {withCredentials: true});
      return response.data;
    });
  }

  static deleteStudent(studentId: string, classroomId: string) {
    return doRequest<any>(async () => {
      const response = await axios.post(`${API_URL}/classrooms/${classroomId}/students/delete`, { studentId }, {withCredentials: true});
      return response.data;
    });
  }

  static deleteClassroom(classroomId: string) {
    return doRequest<IClassroom>(async () => {
      const response = await axios.post(`${API_URL}/classrooms/${classroomId}/delete`, null, {withCredentials: true});
      return response.data;
    });
  }
}