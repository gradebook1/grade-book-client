import axios from "axios";
import { IUserData } from "../app/userSlice";
import { API_URL, doRequest } from "./api.config";
import { IGroupPopulated } from "./groups.api";

export interface ITask {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  date: string;
  course?: string;
  isPrivate: boolean;
  privateStudents?: IUserData[];
  grades: IGrade[];
  // urlCode: string;
}

export interface IGrade {
  student: string;
  grade?: number | null;
}

export enum GradeCalculationType {
  AVERAGE = 'average',
  SUM = 'sum',
}

export interface ICreateTaskDTO {
  name: string;
  date: string;
  course?: string;
  isPrivate: boolean;
  privateStudents?: string[];
}

export interface IDeleteTaskDTO {
  course: string;
  task: string;
}

export interface IGetAllTasksResponseDTO {
  groups: IGroupPopulated[];
  tasks: ITask[];
}

export interface IUpdateCourseLevelTaskGradeDTO {
  course: string;
  task: string;
  student: string;
  grade?: number;
}

export default class TasksAPI {
  static createCourseLevelTask(createTaskDTO: ICreateTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/tasks/course-level`, createTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteCourseLevelTask(deleteTaskDTO: IDeleteTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/tasks/course-level/delete`, deleteTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static getAllTasks(courseUrlCode: string, classroomUrlCode: string) {
    return doRequest<IGetAllTasksResponseDTO>(async () => {
      const response = await axios.get(`${API_URL}/groups/${classroomUrlCode}/c/${courseUrlCode}`, {withCredentials: true});
      return response.data;
    });
  }

  static updateCourseLevelTaskGrade(updateCourseLevelTaskGradeDTO: IUpdateCourseLevelTaskGradeDTO) {
    return doRequest<any>(async () => {
      const response = await axios.patch(`${API_URL}/tasks/grade`, updateCourseLevelTaskGradeDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateTask(taskId: string, createTaskDTO: ICreateTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.put(`${API_URL}/tasks/${taskId}`, createTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

}