import axios from "axios";
import { API_URL, doRequest } from "./api.config";
import { ISubgroupPopulated } from "./subgroups.api";
import { GradeCalculationType, ICreateTaskDTO, IDeleteTaskDTO, IGrade, ITask } from "./tasks.api";

export interface IGroup {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  course: string;
  gradeType: GradeCalculationType;
  grades: IGrade[];
  subgroups: string[];
  tasks: string[];
}

export interface IGroupPopulated {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  course: string;
  gradeType: GradeCalculationType;
  grades: IGrade[];
  subgroups: ISubgroupPopulated[];
  tasks: ITask[];
}

export interface ICreateGroupDTO {
  name: string;
  course: string;
  gradeType: GradeCalculationType;
}

export interface IDeleteGroupDTO {
  course: string;
}

export interface IUpdateGroupGradeDTO {
  course: string;
  group: string;
  student: string;
  grade?: number;
}

export default class GroupsAPI {
  static createGroup(createGroupDTO: ICreateGroupDTO) {
    return doRequest<IGroup>(async () => {
      const response = await axios.post(`${API_URL}/groups`, createGroupDTO, {withCredentials: true});
      return response.data;
    });
  }

  static createTask(groupId: string, createTaskDTO: ICreateTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/groups/${groupId}/tasks`, createTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteTask(groupId: string, deleteTaskDTO: IDeleteTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/groups/${groupId}/tasks/delete`, deleteTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateGroupGrade(updateGroupGradeDTO: IUpdateGroupGradeDTO) {
    return doRequest<any>(async () => {
      const response = await axios.patch(`${API_URL}/groups/grade`, updateGroupGradeDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateGroup(groupId: string, createGroupDTO: ICreateGroupDTO) {
    return doRequest<IGroup>(async () => {
      const response = await axios.put(`${API_URL}/groups/${groupId}`, createGroupDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteGroup(groupId: string, deleteGroupDTO: IDeleteGroupDTO) {
    return doRequest<IGroup>(async () => {
      const response = await axios.post(`${API_URL}/groups/${groupId}/delete`, deleteGroupDTO, {withCredentials: true});
      return response.data;
    });
  }
}