import axios from "axios";
import { ICoursePopulated, UserCourseRole } from "../app/courseSlice";
import { API_URL, doRequest } from "./api.config";

export interface ICourse {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  classroom: string;
  teacher: string;
  isPrivate: boolean;
  privateStudents?: string[];
  urlCode: string;
}

export interface ICreateCourseDTO {
  name: string;
  classroom: string;
  teacher: string;
  isPrivate: boolean;
  privateStudents?: string[];
}

export interface ICourseShortInfo {
  _id: string;
  name: string;
  urlCode: string;
}

export interface IGetCoursePageInfoResponseDto {
  course: ICoursePopulated;
  role: UserCourseRole;
}

export interface IUpdateCourseGradeDTO {
  course: string;
  student: string;
  grade?: number;
}

export interface IDeleteCourseDTO {
  classroom: string;
}

export default class CoursesAPI {
  static createCourse(createCourseDTO: ICreateCourseDTO) {
    return doRequest<ICourse>(async () => {
      const response = await axios.post(`${API_URL}/courses`, createCourseDTO, {withCredentials: true});
      return response.data;
    });
  }

  static getInfoForCoursePage(courseUrlCode: string, classroomUrlCode: string) {
    return doRequest<IGetCoursePageInfoResponseDto>(async () => {
      const response = await axios.get(`${API_URL}/courses/${classroomUrlCode}/c/${courseUrlCode}`, {withCredentials: true});
      return response.data;
    });
  }

  static updateCourse(courseId: string, createCourseDTO: ICreateCourseDTO) {
    return doRequest<ICourse>(async () => {
      const response = await axios.put(`${API_URL}/courses/${courseId}`, createCourseDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateCourseGrade(updateCourseGradeDTO: IUpdateCourseGradeDTO) {
    return doRequest<any>(async () => {
      const response = await axios.patch(`${API_URL}/courses/grade`, updateCourseGradeDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteCourse(courseId: string, deleteCourseDTO: IDeleteCourseDTO) {
    return doRequest<ICourse>(async () => {
      const response = await axios.post(`${API_URL}/courses/${courseId}/delete`, deleteCourseDTO, {withCredentials: true});
      return response.data;
    });
  }
}