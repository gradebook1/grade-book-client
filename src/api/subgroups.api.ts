import axios from "axios";
import { API_URL, doRequest } from "./api.config";
import { IModulePopulated } from "./modules.api";
import { GradeCalculationType, ICreateTaskDTO, IDeleteTaskDTO, IGrade, ITask } from "./tasks.api";

export interface ISubgroup {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  gradeType: GradeCalculationType;
  grades: IGrade[];
  modules: string[];
  tasks: string[];
}

export interface ISubgroupPopulated {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  gradeType: GradeCalculationType;
  grades: IGrade[];
  modules: IModulePopulated[];
  tasks: ITask[];
}

export interface ICreateSubgroupDTO {
  name: string;
  course: string;
  group: string;
  gradeType: GradeCalculationType;
}

export interface IDeleteSubgroupDTO {
  course: string;
  group: string;
}

export interface IUpdateSubgroupGradeDTO {
  course: string;
  subgroup: string;
  student: string;
  grade?: number;
}

export default class SubgroupsAPI {
  static createSubgroup(createSubgroupDTO: ICreateSubgroupDTO) {
    return doRequest<ISubgroup>(async () => {
      const response = await axios.post(`${API_URL}/subgroups`, createSubgroupDTO, {withCredentials: true});
      return response.data;
    });
  }

  static createTask(subgroupId: string, createTaskDTO: ICreateTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/subgroups/${subgroupId}/tasks`, createTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteTask(subgroupId: string, deleteTaskDTO: IDeleteTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/subgroups/${subgroupId}/tasks/delete`, deleteTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateSubgroupGrade(updateSubgroupGradeDTO: IUpdateSubgroupGradeDTO) {
    return doRequest<any>(async () => {
      const response = await axios.patch(`${API_URL}/subgroups/grade`, updateSubgroupGradeDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateSubgroup(subgroupId: string, createSubgroupDTO: ICreateSubgroupDTO) {
    return doRequest<ISubgroup>(async () => {
      const response = await axios.put(`${API_URL}/subgroups/${subgroupId}`, createSubgroupDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteSubgroup(subgroupId: string, deleteSubgroupDTO: IDeleteSubgroupDTO) {
    return doRequest<ISubgroup>(async () => {
      const response = await axios.post(`${API_URL}/subgroups/${subgroupId}/delete`, deleteSubgroupDTO, {withCredentials: true});
      return response.data;
    });
  }
}
