import axios from "axios";
import { IUserData } from "../app/userSlice";
import { API_URL, doRequest } from "./api.config";

export interface UserData {
  email: string,
  password: string,
}

export interface ICreateUserDTO {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
}

export interface IUpdateUserDTO {
  email: string;
  firstName: string;
  lastName: string;
}

export interface IUpdatePasswordDTO {
  oldPassword: string;
  newPassword: string;
}

export default class AuthAPI {
  static async login(userData: UserData) {
    // try {
    //   const response = await axios.post(`${API_URL}/auth/login`, userData, {withCredentials: true});
    //   return { data: response.data, error: null };
    // } catch (error) {
    //   return { data: null, error };
    // }
    return doRequest<any>(async () => {
      const response = await axios.post(`${API_URL}/auth/login`, userData, {withCredentials: true});
      return response.data;
    });
  }

  static register(userData: ICreateUserDTO) {
    return doRequest<any>(async () => {
      const response = await axios.post(`${API_URL}/auth/register`, userData, {withCredentials: true});
      return response.data;
    });
  }

  static updateProfile(userData: IUpdateUserDTO) {
    return doRequest<IUserData>(async () => {
      const response = await axios.put(`${API_URL}/profile`, userData, {withCredentials: true});
      return response.data;
    });
  }

  static updatePassword(userPasswordData: IUpdatePasswordDTO) {
    return doRequest<IUserData>(async () => {
      const response = await axios.patch(`${API_URL}/profile/password`, userPasswordData, {withCredentials: true});
      return response.data;
    });
  }
}