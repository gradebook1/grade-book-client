import axios from "axios";
import { API_URL, doRequest } from "./api.config";
import { GradeCalculationType, ICreateTaskDTO, IDeleteTaskDTO, IGrade, ITask } from "./tasks.api";

export interface IModule {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  gradeType: GradeCalculationType;
  grades: IGrade[];
  tasks: string[];
}

export interface IModulePopulated {
  _id: string;
  name: string;
  description?: string;
  createdDate: string;
  gradeType: GradeCalculationType;
  grades: IGrade[];
  tasks: ITask[];
}

export interface ICreateModuleDTO {
  name: string;
  course: string;
  subgroup: string;
  gradeType: GradeCalculationType;
}

export interface IDeleteModuleDTO {
  course: string;
  subgroup: string;
}

export interface IUpdateModuleGradeDTO {
  course: string;
  module: string;
  student: string;
  grade?: number;
}

export default class ModulesAPI {
  static createModule(createModuleDTO: ICreateModuleDTO) {
    return doRequest<IModule>(async () => {
      const response = await axios.post(`${API_URL}/modules`, createModuleDTO, {withCredentials: true});
      return response.data;
    });
  }

  static createTask(moduleId: string, createTaskDTO: ICreateTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/modules/${moduleId}/tasks`, createTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteTask(moduleId: string, deleteTaskDTO: IDeleteTaskDTO) {
    return doRequest<ITask>(async () => {
      const response = await axios.post(`${API_URL}/modules/${moduleId}/tasks/delete`, deleteTaskDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateModuleGrade(updateModuleGradeDTO: IUpdateModuleGradeDTO) {
    return doRequest<any>(async () => {
      const response = await axios.patch(`${API_URL}/modules/grade`, updateModuleGradeDTO, {withCredentials: true});
      return response.data;
    });
  }

  static updateModule(moduleId: string, createModuleDTO: ICreateModuleDTO) {
    return doRequest<IModule>(async () => {
      const response = await axios.put(`${API_URL}/modules/${moduleId}`, createModuleDTO, {withCredentials: true});
      return response.data;
    });
  }

  static deleteModule(moduleId: string, deleteModuleDTO: IDeleteModuleDTO) {
    return doRequest<IModule>(async () => {
      const response = await axios.post(`${API_URL}/modules/${moduleId}/delete`, deleteModuleDTO, {withCredentials: true});
      return response.data;
    });
  }
}
