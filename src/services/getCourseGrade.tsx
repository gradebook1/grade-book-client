import { IGroup, IGroupPopulated } from "../api/groups.api";
import { IModulePopulated } from "../api/modules.api";
import { ISubgroupPopulated } from "../api/subgroups.api";
import { GradeCalculationType, ITask } from "../api/tasks.api";
import { IUserData } from "../app/userSlice";

// TODO: Add here later groups grades as well
// NOTE: This is for student user
export function getCourseGrade(gradeType: GradeCalculationType, tasks?: ITask[] | null) {
  if (!tasks) return null;

  let count = 0;
  const sum = tasks.reduce((acc: number | null, val) => {
    if (val.grades[0]?.grade === undefined || val.grades[0]?.grade === null) {
      return acc;
    }
    count++;
    if (acc === null) {
      return val.grades[0].grade;
    } else {
      return acc! + val.grades[0]?.grade;
    }
  }, null);

  if (sum && count > 0) {
    if (gradeType === GradeCalculationType.AVERAGE) {
      return +(sum / count).toFixed(2);
    } else if (gradeType === GradeCalculationType.SUM) {
      return sum;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

// function calcGroups(gradeType: GradeCalculationType, groups?: IGroup[] | null) {
  // calculates groups grades per user ([0])
  // uses calcTasks and calcSubGroups to get calculated (or real) grade
// }

// every calc function gets array of entities and returns object {sum: number, count: number}

// so we will have calc functions and get${EntityName}Grade functions will use calc functions and then return one grade which will be then used for calc function on the upper level

function calcTasks(tasks?: ITask[] | null) {
  if (!tasks) {
    return { sum: 0, count: 0 };
  }
  let count = 0;
  const sum = tasks.reduce((acc: number | null, val) => {
    if (val.grades[0]?.grade === undefined || val.grades[0]?.grade === null) {
      return acc;
    }
    count++;
    if (acc === null) {
      return val.grades[0].grade;
    } else {
      return acc! + val.grades[0]?.grade;
    }
  }, null);

  return { sum, count };
}

function calcGroups(groups?: IGroupPopulated[] | null) {
  if (!groups) {
    return { sum: 0, count: 0 };
  }
  let count = 0;
  const sum = groups.reduce((acc: number | null, val) => {
    if (val.grades[0]?.grade === undefined || val.grades[0]?.grade === null) {
      // If no grade for that group
      const grade = getUserGroupGrade(val.gradeType, val.tasks, val.subgroups);
      if (grade === null) {
        return acc;
      } else {
        count++;
        if (acc === null) {
          return grade;
        } else {
          return acc! + grade;
        }
      }
    }
    count++;
    if (acc === null) {
      return val.grades[0].grade;
    } else {
      return acc! + val.grades[0]?.grade;
    }
  }, null);

  return { sum, count };
}

function calcSubgroups(subgroups?: ISubgroupPopulated[] | null) {
  if (!subgroups) {
    return { sum: 0, count: 0 };
  }
  let count = 0;
  const sum = subgroups.reduce((acc: number | null, val) => {
    if (val.grades[0]?.grade === undefined || val.grades[0]?.grade === null) {
      // If no grade for that subgroup
      const grade = getUserSubgroupGrade(val.gradeType, val.tasks, val.modules);
      if (grade === null) {
        return acc;
      } else {
        count++;
        if (acc === null) {
          return grade;
        } else {
          return acc! + grade;
        }
      }
    }
    count++;
    if (acc === null) {
      return val.grades[0].grade;
    } else {
      return acc! + val.grades[0]?.grade;
    }
  }, null);

  return { sum, count };
}

function calcModules(modules?: IModulePopulated[] | null) {
  if (!modules) {
    return { sum: 0, count: 0 };
  }
  let count = 0;
  const sum = modules.reduce((acc: number | null, val) => {
    if (val.grades[0]?.grade === undefined || val.grades[0]?.grade === null) {
      // If no grade for that module
      const grade = getUserModuleGrade(val.gradeType, val.tasks);
      if (grade === null) {
        return acc;
      } else {
        count++;
        if (acc === null) {
          return grade;
        } else {
          return acc! + grade;
        }
      }
    }
    count++;
    if (acc === null) {
      return val.grades[0].grade;
    } else {
      return acc! + val.grades[0]?.grade;
    }
  }, null);

  return { sum, count };
}

function calcGrade(sum: number | null, count: number, gradeType: GradeCalculationType) {
  if (sum !== null && count > 0) {
    if (gradeType === GradeCalculationType.AVERAGE) {
      return +(sum / count).toFixed(2);
    } else if (gradeType === GradeCalculationType.SUM) {
      return sum;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function getUserCourseGrade(
  courseGradeType: GradeCalculationType,
  userOnlyTasks?: ITask[] | null,
  userOnlyGroups?: IGroupPopulated[] | null,
) {
  if (!userOnlyTasks && !userOnlyGroups) return null;

  const { sum: tasksSum, count: tasksCount } = calcTasks(userOnlyTasks);
  const { sum: groupsSum, count: groupsCount } = calcGroups(userOnlyGroups);

  const sum = (tasksSum || 0) + (groupsSum || 0);
  const count = tasksCount + groupsCount;

  const grade = calcGrade(sum, count, courseGradeType);

  return grade;
}

export function getUserGroupGrade(groupGradeType: GradeCalculationType, userOnlyGroupTasks?: ITask[] | null, userOnlySubgroups?: ISubgroupPopulated[] | null) {
  if (!userOnlyGroupTasks && !userOnlySubgroups) return null;

  const { sum: tasksSum, count: tasksCount } = calcTasks(userOnlyGroupTasks);
  const { sum: subgroupsSum, count: subgroupsCount } = calcSubgroups(userOnlySubgroups);

  const sum = (tasksSum || 0) + (subgroupsSum || 0);
  const count = tasksCount + subgroupsCount;

  const grade = calcGrade(sum, count, groupGradeType);

  return grade;
}

export function getUserSubgroupGrade(subgroupGradeType: GradeCalculationType, userOnlySubgroupTasks?: ITask[] | null, userOnlyModules?: IModulePopulated[] | null) {
  if (!userOnlySubgroupTasks && !userOnlyModules) return null;

  const { sum: tasksSum, count: tasksCount } = calcTasks(userOnlySubgroupTasks);
  const { sum: modulesSum, count: modulesCount } = calcModules(userOnlyModules);

  const sum = (tasksSum || 0) + (modulesSum || 0);
  const count = tasksCount + modulesCount;

  const grade = calcGrade(sum, count, subgroupGradeType);

  return grade;
}

export function getUserModuleGrade(moduleGradeType: GradeCalculationType, userOnlyModuleTasks?: ITask[] | null) {
  if (!userOnlyModuleTasks) return null;

  const { sum: tasksSum, count: tasksCount } = calcTasks(userOnlyModuleTasks);

  const sum = tasksSum || 0;
  const count = tasksCount;

  const grade = calcGrade(sum, count, moduleGradeType);

  return grade;
}

export function getUserOnlyTasksAndOther(user: IUserData, tasks: ITask[], groups: IGroupPopulated[]) {
  const filteredTasks = filterTasksByUser(tasks, user);
  const filteredGroups = filterGroupsByUser(groups, user);

  return { groups: filteredGroups, tasks: filteredTasks };
}

/* FILTER FUNCTONS */

function filterTasksByUser(tasksTofilter: ITask[], user: IUserData) {
  let tasks = [...tasksTofilter];
  tasks = tasks.map(task => {
    return {
      ...task,
      grades: [...task.grades],
    };
  });

  tasks = tasks.filter((task) => {
    if (!task.isPrivate) {
      return true;
    }
    if (
      task.privateStudents?.find(
        (ps) => ps._id.toString() === user._id.toString(),
      )
    ) {
      return true;
    } else {
      return false;
    }
  });
  tasks.forEach((task) => {
    task.grades = task.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return tasks;
}

function filterGroupsByUser(groupsTofilter: IGroupPopulated[], user: IUserData) {
  let groups = [...groupsTofilter];
  groups = groups.map(group => {
    return {
      ...group,
      grades: [...group.grades],
      tasks: [...group.tasks],
      subgroups: [...group.subgroups],
    };
  });

  groups.forEach((group) => {
    group.tasks = filterTasksByUser(group.tasks, user);
    group.subgroups = filterSubgroupsByUser(group.subgroups, user);
    group.grades = group.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return groups;
}

function filterSubgroupsByUser(subgroupsToFilter: ISubgroupPopulated[], user: IUserData) {
  let subgroups = [...subgroupsToFilter];
  subgroups = subgroups.map(subgroup => {
    return {
      ...subgroup,
      grades: [...subgroup.grades],
      tasks: [...subgroup.tasks],
      modules: [...subgroup.modules],
    };
  });

  subgroups.forEach((subgroup) => {
    subgroup.tasks = filterTasksByUser(subgroup.tasks, user);
    subgroup.modules = filterModulesByUser(subgroup.modules, user);
    subgroup.grades = subgroup.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return subgroups;
}

function filterModulesByUser(modulesToFilter: IModulePopulated[], user: IUserData) {
  let modules = [...modulesToFilter];
  modules = modules.map(module => {
    return {
      ...module,
      grades: [...module.grades],
      tasks: [...module.tasks],
    };
  });

  modules.forEach((module) => {
    module.tasks = filterTasksByUser(module.tasks, user);
    module.grades = module.grades.filter((grade) => {
      return grade.student.toString() === user._id.toString();
    });
  });

  return modules;
}

// export function getCourseGradeAll(users: IUserData[], gradeType: GradeCalculationType, tasks?: ITask[] | null) {
//   if (!tasks) return null;


// }
